## pg-stream-cli

A limited-functionality PostgreSQL client utility for simple queries and logical replication streaming using the pgoutput protocol.

This utility is basically a thin wrapper on top of the [pg-stream-core](https://www.npmjs.com/package/@jmorecroft67/pg-stream-core) and [pg-stream-gcp](https://www.npmjs.com/package/@jmorecroft67/pg-stream-gcp) libraries. If you'd like to understand the implementation, or see examples of the kind of output to expect, please see the README for these libs.

### usage

```bash
$ pg-stream --help
```

```
Usage: pg-stream [options] [command]

Simple PostgreSQL client for querying and logical replication streaming.

Options:
  -V, --version    output the version number
  -h, --help       display help for command

Commands:
  query            Send an SQL query to the PostgreSQL server.
  recvlogical      Send a logical replication stream from the PostgreSQL server to stdout, stderr or a file.
  recvlogical-gcp  Send a logical replication stream from the PostgreSQL server to a Google Cloud Platform (GCP) PubSub topic.
  help [command]   display help for command
```

#### query

```bash
$ pg-stream query --help
```

```
Usage: pg-stream-query [options]

Options:
  -h, --host <host>          database server host (default: "localhost", env: PGHOST)
  -p, --port <port>          database server port (default: "5432", env: PGPORT)
  -U, --username <user>      database user name (default: "jessmorecroft", env: PGUSER)
  -W, --password <password>  database user password (env: PGPASSWORD)
  -d, --dbname <dbname>      database name to connect to (default: "postgres", env: PGDATABASE)
  --noSSL                    do not connect using SSL
  --verbose                  debug logging
  -q, --quiet                minimal logging
  -c, --command <sql>        run a command and exit; multiple may be specified and will be executed in sequence
  --help                     display help for command
```

#### recvlogical

```bash
$ pg-stream recvlogical --help
```

```
Usage: pg-stream-recvlogical [options]

Options:
  -h, --host <host>                database server host (default: "localhost", env: PGHOST)
  -p, --port <port>                database server port (default: "5432", env: PGPORT)
  -U, --username <user>            database user name (default: "jessmorecroft", env: PGUSER)
  -W, --password <password>        database user password (env: PGPASSWORD)
  -d, --dbname <dbname>            database name to connect to (default: "postgres", env: PGDATABASE)
  --noSSL                          do not connect using SSL
  --verbose                        debug logging
  -q, --quiet                      minimal logging
  -S, --slot <slot>                name of the logical replication slot
  -P, --publication <publication>  name of the pgoutput publication
  --createTempSlotIfNone           create a temporary slot if one does not already exist (default: false)
  --createSlotIfNone               create a persisted slot if one does not already exist (default: false)
  --createPublicationIfNone        create a publication if one does not already exist (default: false)
  -I, --includeTable <tables>      none specified (all), one or many tables for any newly created publication to filter
                                   (default: [])
  --retryOptions <retry-options>   retry options for retrying logical streaming on connection error to Postgres; may
                                   be "false" if no retries should be attempted (default:
                                   {"delay":1000,"exponentialBackoff":{"capDelay":10000},"maxRetries":5})
  -o, --output <dest>              output xlogs to stdout or stderr (choices: "stdout", "stderr")
  -f, --file <filename>            output xlogs to file
  --help                           display help for command
```

#### recvlogical-gcp

```bash
$ pg-stream recvlogical-gcp --help
```

```
Usage: pg-stream-recvlogical-gcp [options]

Options:
  -h, --host <host>                   database server host (default: "localhost", env: PGHOST)
  -p, --port <port>                   database server port (default: "5432", env: PGPORT)
  -U, --username <user>               database user name (default: "jessmorecroft", env: PGUSER)
  -W, --password <password>           database user password (env: PGPASSWORD)
  -d, --dbname <dbname>               database name to connect to (default: "postgres", env: PGDATABASE)
  --noSSL                             do not connect using SSL
  --verbose                           debug logging
  -q, --quiet                         minimal logging
  -S, --slot <slot>                   name of the logical replication slot
  -P, --publication <publication>     name of the pgoutput publication
  --createTempSlotIfNone              create a temporary slot if one does not already exist (default: false)
  --createSlotIfNone                  create a persisted slot if one does not already exist (default: false)
  --createPublicationIfNone           create a publication if one does not already exist (default: false)
  -I, --includeTable <tables>         none specified (all), one or many tables for any newly created publication to filter (default: [])
  --retryOptions <retry-options>      retry options for retrying logical streaming on connection error to Postgres; may be "false" if no retries should be
                                      attempted (default: {"delay":1000,"exponentialBackoff":{"capDelay":10000},"maxRetries":5})
  -t, --topicName <topic-name>        output xlogs to GCP PubSub topic
  --projectId <project-name>          GCP project name (env: PUBSUB_PROJECT_ID)
  --includeAll                        include all xlogs, not just table-related (default: false)
  --noOrdering                        do not guarantee preservation of message ordering (default: false)
  --concurrency <concurrency>         max concurrency of xlog handler (default: "5")
  -A, --attribute <field>             optionally fields taken from inserted, updated or deleted records to be set as attributes in published message (default:
                                      [])
  -K, --orderingKeySuffix <field>     optionally fields taken from inserted, updated or deleted records, along with table name, to construct the ordering key
                                      (default: [])
  --sendRetryOptions <retry-options>  retry options for send retry on error from the GCP PubSub service; may be "false" if no retries should be attempted
                                      (default: {"delay":1000,"exponentialBackoff":{"capDelay":10000},"maxRetries":5})
  --help                              display help for command
```
