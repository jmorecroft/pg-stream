import { Command, Option } from 'commander';
import { env } from 'process';
import * as t from 'io-ts';
import { IntFromString } from 'io-ts-types';
import { createLogger, stdSerializers } from 'bunyan';
import {
  capDelay,
  constantDelay,
  exponentialBackoff,
  limitRetries,
  Monoid,
  RetryPolicy
} from 'retry-ts';
import * as O from 'fp-ts/Option';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import { Logger, PgConnectError } from '@jmorecroft67/pg-stream-core';
import { retrying } from 'retry-ts/lib/Task';
import { pipe } from 'fp-ts/lib/function';

export const getLogger = ({
  quiet,
  verbose
}: { quiet?: boolean; verbose?: boolean } = {}) =>
  createLogger({
    name: 'cli',
    level: verbose ? 'debug' : quiet ? 'error' : 'info',
    serializers: stdSerializers
  });

export const BaseOptions = t.intersection([
  t.type({
    host: t.string,
    port: IntFromString,
    username: t.string,
    password: t.string,
    dbname: t.string
  }),
  t.partial({
    noSSL: t.boolean,
    verbose: t.boolean,
    quiet: t.boolean
  })
]);

export const RetryOptions = t.union([
  t.literal(false),
  t.intersection([
    t.type({
      delay: t.number
    }),
    t.partial({
      exponentialBackoff: t.partial({
        capDelay: t.number
      }),
      maxCumulativeDelay: t.number,
      maxRetries: t.number
    })
  ])
]);

type RetryOptions = t.TypeOf<typeof RetryOptions>;

export const RecvlogicalOptions = t.intersection([
  BaseOptions,
  t.type({
    slot: t.string,
    publication: t.string,
    createTempSlotIfNone: t.boolean,
    createSlotIfNone: t.boolean,
    createPublicationIfNone: t.boolean,
    includeTable: t.array(t.string),
    retryOptions: RetryOptions
  })
]);

export const DEFAULT_RETRY_OPTIONS: RetryOptions = {
  delay: 1000, // 1 second wait time initially
  exponentialBackoff: { capDelay: 10000 }, // exponentially increase to 1 minute max
  maxRetries: 5 // max 5 retries
};

export const addBaseOptions = (command: Command): Command => {
  command
    .addOption(
      new Option('-h, --host <host>', 'database server host')
        .default('localhost')
        .env('PGHOST')
    )
    .addOption(
      new Option('-p, --port <port>', 'database server port')
        .default('5432')
        .env('PGPORT')
    );

  const usernameOpt = new Option('-U, --username <user>', 'database user name')
    .makeOptionMandatory(true)
    .env('PGUSER');

  if (env.LOGNAME) {
    usernameOpt.default(env.LOGNAME);
  }

  return command
    .addOption(usernameOpt)
    .addOption(
      new Option('-W, --password <password>', 'database user password')
        .makeOptionMandatory(true)
        .env('PGPASSWORD')
    )
    .addOption(
      new Option('-d, --dbname <dbname>', 'database name to connect to')
        .default('postgres')
        .makeOptionMandatory(true)
        .env('PGDATABASE')
    )
    .option('--noSSL', 'do not connect using SSL')
    .option('--verbose', 'debug logging')
    .addOption(
      new Option('-q, --quiet', 'minimal logging').conflicts('verbose')
    );
};

export const addRecvlogicalOptions = (command: Command): Command => {
  return addBaseOptions(command)
    .requiredOption('-S, --slot <slot>', 'name of the logical replication slot')
    .requiredOption(
      '-P, --publication <publication>',
      'name of the pgoutput publication'
    )
    .option(
      '--createTempSlotIfNone',
      'create a temporary slot if one does not already exist',
      false
    )
    .addOption(
      new Option(
        '--createSlotIfNone',
        'create a persisted slot if one does not already exist'
      )
        .default(false)
        .conflicts('createTempSlotIfNone')
    )
    .option(
      '--createPublicationIfNone',
      'create a publication if one does not already exist',
      false
    )
    .option(
      '-I, --includeTable <tables>',
      'none specified (all), one or many tables for any newly created publication to filter',
      (value, prev) => [...prev, value],
      [] as string[]
    )
    .addOption(
      new Option(
        '--retryOptions <retry-options>',
        'retry options for retrying logical streaming on connection error to Postgres; may be "false" if no retries should be attempted'
      )
        .default(DEFAULT_RETRY_OPTIONS)
        .argParser(JSON.parse)
    );
};

const makeRetryPolicy = (options: RetryOptions) => {
  if (options) {
    let policy = options.exponentialBackoff
      ? exponentialBackoff(options.delay)
      : constantDelay(options.delay);

    if (options.exponentialBackoff?.capDelay) {
      policy = capDelay(options.exponentialBackoff.capDelay, policy);
    }

    if (options.maxCumulativeDelay) {
      policy = Monoid.concat(
        limitCumulativeDelay(options.maxCumulativeDelay),
        policy
      );
    }

    if (options.maxRetries) {
      policy = Monoid.concat(limitRetries(options.maxRetries), policy);
    }

    return policy;
  }
  return () => O.none;
};

const limitCumulativeDelay =
  (maxCumulativeDelay: number): RetryPolicy =>
  (status) =>
    status.cumulativeDelay > maxCumulativeDelay ? O.none : O.some(0);

export const runPgTask =
  (
    task: TE.TaskEither<Error, void>
  ): RTE.ReaderTaskEither<
    { taskName: string; retryOptions: RetryOptions; logger: Logger },
    Error,
    void
  > =>
  ({ taskName, logger, retryOptions }) =>
    retrying(
      makeRetryPolicy(retryOptions),
      (status) => {
        const { iterNumber: retryAttempt, cumulativeDelay } = status;
        if (retryAttempt > 0) {
          logger.info(
            { retryAttempt, cumulativeDelay },
            `Retrying ${taskName} after earlier connection error.`
          );
        }
        return task;
      },
      (result) => {
        return pipe(
          result,
          E.fold(
            (e) =>
              e instanceof PgConnectError &&
              ['ECONNREFUSED', 'ECONNRESET', 'ETIMEDOUT'].includes(
                (e.cause as NodeJS.ErrnoException).code ?? ''
              ),
            () => false
          )
        );
      }
    );
