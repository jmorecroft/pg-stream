import { Command } from 'commander';
import findPackageJson = require('find-package-json');

const program = new Command();

const version = Array.from(findPackageJson()).at(0)?.version;

if (version) {
  program.version(version);
}

program
  .name('pg-stream')
  .description(
    'Simple PostgreSQL client for querying and logical replication streaming.'
  )
  .command('query', 'Send an SQL query to the PostgreSQL server.')
  .command(
    'recvlogical',
    'Send a logical replication stream from the PostgreSQL server to stdout, stderr or a file.'
  )
  .command(
    'recvlogical-gcp',
    'Send a logical replication stream from the PostgreSQL server to a Google Cloud Platform (GCP) PubSub topic.'
  );

program.parse(process.argv);
