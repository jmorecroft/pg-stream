import { Command, Option } from 'commander';
import {
  addRecvlogicalOptions,
  getLogger,
  RecvlogicalOptions,
  runPgTask
} from './utils';
import * as t from 'io-ts';
import * as pg from '@jmorecroft67/pg-stream-core';
import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import { formatValidationErrors } from 'io-ts-reporters';
import {
  fileHandler,
  stderrHandler,
  stdoutHandler
} from '@jmorecroft67/pg-stream-core';
import { exit } from 'process';
import * as _ from 'lodash';

const program = addRecvlogicalOptions(new Command())
  .addOption(
    new Option(
      '-o, --output <dest>',
      'output xlogs to stdout or stderr'
    ).choices(['stdout', 'stderr'])
  )
  .addOption(
    new Option('-f, --file <filename>', 'output xlogs to file').conflicts(
      'output'
    )
  );

program.parse(process.argv);

const Options = t.intersection([
  RecvlogicalOptions,
  t.partial({
    output: t.union([t.literal('stderr'), t.literal('stdout')]),
    file: t.string
  })
]);

const decoded = Options.decode(program.opts());
if (E.isLeft(decoded)) {
  // eslint-disable-next-line no-console
  console.error('Bad input:', formatValidationErrors(decoded.left).join('\n'));
  program.help({ error: true });
} else {
  const { password, ...config } = decoded.right;

  const {
    host,
    port,
    username,
    dbname,
    noSSL,
    verbose,
    quiet,
    slot,
    publication,
    createTempSlotIfNone,
    createSlotIfNone,
    createPublicationIfNone,
    includeTable,
    retryOptions,
    output,
    file
  } = config;

  const logger = getLogger({ quiet, verbose });

  logger.info({ config }, 'Parsed args and applied env values and defaults.');

  const includeTables = includeTable.length > 0 ? includeTable : undefined;

  const xlogs = file
    ? { handler: fileHandler(file) }
    : output === 'stderr'
    ? { handler: stderrHandler }
    : { handler: stdoutHandler };

  const options: pg.RecvlogicalOptions = {
    host,
    port,
    database: dbname,
    username,
    password,
    useSSL: noSSL === undefined ? undefined : !noSSL,
    slotName: slot,
    publicationName: publication,
    createTempSlotIfNone,
    createSlotIfNone,
    createPublicationIfNone,
    includeTables,
    xlogs
  };

  const task = TE.bracket(
    pg.open({ options, logger }),
    (conn) =>
      pipe(
        { conn, logger },
        pipe(
          pg.start(options, true),
          RTE.chain(() => pg.recvlogical(options))
        )
      ),
    (conn) => pg.close({ conn, logger })
  );

  const taskName = 'logical streaming';

  runPgTask(task)({ taskName, retryOptions, logger })().then((result) =>
    pipe(
      result,
      E.fold(() => exit(1), _.noop)
    )
  );
}
