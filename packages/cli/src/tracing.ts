import '@opentelemetry/api';
import { Resource } from '@opentelemetry/resources';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base';
import { env } from 'process';
import { JaegerExporter } from '@opentelemetry/exporter-jaeger';
import { TraceExporter } from '@google-cloud/opentelemetry-cloud-trace-exporter';
import findPackageJson = require('find-package-json');

const packageJson = Array.from(findPackageJson()).at(0);

const serviceName = 'pg-stream';
const serviceVersion = packageJson?.version ?? 'unknown';

if (env.OTEL_EXPORTER_GCLOUD || env.OTEL_EXPORTER_JAEGER_AGENT_HOST) {
  const resource = Resource.default().merge(
    new Resource({
      [SemanticResourceAttributes.SERVICE_NAME]: serviceName,
      [SemanticResourceAttributes.SERVICE_VERSION]: serviceVersion
    })
  );

  const provider = new NodeTracerProvider({
    resource
  });

  const exporter = env.OTEL_EXPORTER_GCLOUD
    ? new TraceExporter()
    : new JaegerExporter();

  const processor = new BatchSpanProcessor(exporter);
  provider.addSpanProcessor(processor);
  provider.register();
}
