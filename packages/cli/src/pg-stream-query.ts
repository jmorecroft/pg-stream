/* eslint-disable no-console */
import { Command } from 'commander';
import { addBaseOptions, BaseOptions, getLogger } from './utils';
import * as t from 'io-ts';
import * as pg from '@jmorecroft67/pg-stream-core';
import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import { formatValidationErrors } from 'io-ts-reporters';
import { exit } from 'process';

const program = addBaseOptions(new Command()).requiredOption(
  '-c, --command <sql>',
  'run a command and exit; multiple may be specified and will be executed in sequence',
  (value: string, prev: string[] | undefined) => prev?.concat(value) ?? [value]
);

program.parse(process.argv);

const Options = t.intersection(
  [
    BaseOptions,
    t.type({
      command: t.array(t.string)
    })
  ],
  'options'
);

const decoded = Options.decode(program.opts());
if (E.isLeft(decoded)) {
  console.error('Bad input:', formatValidationErrors(decoded.left).join('\n'));
  program.help({ error: true });
} else {
  const { password, ...config } = decoded.right;

  const { command, host, port, username, dbname, noSSL, verbose, quiet } =
    config;

  const logger = getLogger({ quiet, verbose });

  logger.info({ config }, 'Parsed args and applied env values and defaults.');

  const options: pg.BaseOptions = {
    host,
    port,
    database: dbname,
    username,
    password,
    useSSL: noSSL === undefined ? undefined : !noSSL
  };

  TE.bracket(
    pg.open({ options, logger }),
    (conn) =>
      pipe(
        { conn, logger },
        pipe(
          pg.start(options, false),
          RTE.bindTo('startup'),
          RTE.bind('results', () =>
            RTE.sequenceSeqArray(command.map((c) => pg.query(c)))
          )
        )
      ),
    (conn) => pg.close({ conn, logger })
  )().then((result) => {
    pipe(
      result,
      E.fold(
        () => exit(1),
        ({ results }) => {
          console.log(
            pg.stringify(results.length === 1 ? results[0] : results)
          );
        }
      )
    );
  });
}
