import './tracing';
import { Command, Option } from 'commander';
import {
  addRecvlogicalOptions,
  DEFAULT_RETRY_OPTIONS,
  getLogger,
  RecvlogicalOptions,
  RetryOptions,
  runPgTask
} from './utils';
import * as t from 'io-ts';
import * as pg from '@jmorecroft67/pg-stream-core';
import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import { formatValidationErrors } from 'io-ts-reporters';
import { pubSubHandler } from '@jmorecroft67/pg-stream-gcp';
import { exit } from 'process';
import * as _ from 'lodash';
import { IntFromString } from 'io-ts-types';

const program = addRecvlogicalOptions(new Command())
  .addOption(
    new Option(
      '-t, --topicName <topic-name>',
      'output xlogs to GCP PubSub topic'
    ).makeOptionMandatory(true)
  )
  .addOption(
    new Option('--projectId <project-name>', 'GCP project name').env(
      'PUBSUB_PROJECT_ID'
    )
  )
  .option('--includeAll', 'include all xlogs, not just table-related', false)
  .option(
    '--noOrdering',
    'do not guarantee preservation of message ordering',
    false
  )
  .option('--concurrency <concurrency>', 'max concurrency of xlog handler', '5')
  .option(
    '-A, --attribute <field>',
    'optionally fields taken from inserted, updated or deleted records to be set as attributes in published message',
    (value, prev) => [...prev, value],
    [] as string[]
  )
  .addOption(
    new Option(
      '-K, --orderingKeySuffix <field>',
      'optionally fields taken from inserted, updated or deleted records, along with table name, to construct the ordering key'
    )
      .argParser<string[]>((value, prev) => [...prev, value])
      .default([] as string[])
      .conflicts('noOrdering')
  )
  .addOption(
    new Option(
      '--sendRetryOptions <retry-options>',
      'retry options for send retry on error from the GCP PubSub service; may be "false" if no retries should be attempted'
    )
      .default(DEFAULT_RETRY_OPTIONS)
      .argParser(JSON.parse)
  );

program.parse(process.argv);

const Options = t.intersection([
  RecvlogicalOptions,
  t.type({
    projectId: t.string,
    topicName: t.string,
    includeAll: t.boolean,
    concurrency: IntFromString,
    attribute: t.array(t.string),
    sendRetryOptions: RetryOptions
  }),
  t.union([
    t.type({
      noOrdering: t.literal(true)
    }),
    t.type({
      noOrdering: t.literal(false),
      orderingKeySuffix: t.array(t.string)
    })
  ])
]);

const decoded = Options.decode(program.opts());
if (E.isLeft(decoded)) {
  // eslint-disable-next-line no-console
  console.error('Bad input:', formatValidationErrors(decoded.left).join('\n'));
  program.help({ error: true });
} else {
  const { password, ...config } = decoded.right;

  const {
    host,
    port,
    username,
    dbname,
    noSSL,
    verbose,
    quiet,
    slot,
    publication,
    createTempSlotIfNone,
    createSlotIfNone,
    createPublicationIfNone,
    includeTable,
    topicName,
    projectId,
    includeAll,
    noOrdering,
    concurrency,
    attribute: attributes,
    retryOptions,
    sendRetryOptions
  } = config;

  const logger = getLogger({ quiet, verbose });

  logger.info({ config }, 'Parsed args and applied env values and defaults.');

  const includeTables = includeTable.length > 0 ? includeTable : undefined;

  const xlogs = {
    handler: pubSubHandler({
      topicName,
      projectId,
      includeAll,
      attributes,
      retryOptions: sendRetryOptions,
      ...(noOrdering
        ? {
            noOrdering
          }
        : {
            noOrdering,
            orderingKeySuffix: config.orderingKeySuffix
          })
    }),
    concurrency
  };

  const options: pg.RecvlogicalOptions = {
    host,
    port,
    database: dbname,
    username,
    password,
    useSSL: noSSL === undefined ? undefined : !noSSL,
    slotName: slot,
    publicationName: publication,
    createTempSlotIfNone,
    createSlotIfNone,
    createPublicationIfNone,
    includeTables,
    xlogs
  };

  const task = TE.bracket(
    pg.open({ options, logger }),
    (conn) =>
      pipe(
        { conn, logger },
        pipe(
          pg.start(options, true),
          RTE.chain(() => pg.recvlogical(options))
        )
      ),
    (conn) => pg.close({ conn, logger })
  );

  const taskName = 'logical streaming';

  runPgTask(task)({ taskName, retryOptions, logger })().then((result) =>
    pipe(
      result,
      E.fold(() => exit(1), _.noop)
    )
  );
}
