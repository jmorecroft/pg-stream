# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.20.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.19.0...v0.20.0) (2022-07-12)


### Bug Fixes

* Give int test a little more time to init. ([4ee0d3c](https://gitlab.com/jmorecroft/pg-stream/commit/4ee0d3cf4741ce1760e727e0e63897663d539ffe))


### Features

* Add basic support for OpenTelemetry tracing with GCP PubSub. ([f46d5f3](https://gitlab.com/jmorecroft/pg-stream/commit/f46d5f3ce42ca98da975ee3a295a2965e7394f12))





# [0.19.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.3...v0.19.0) (2022-06-26)


### Features

* **gcp:** Added txn info (id, timestamp) to message attributes. ([bd0c048](https://gitlab.com/jmorecroft/pg-stream/commit/bd0c048b1bd6a5c150e7f48715dbd8653c15e946))





## [0.18.3](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.2...v0.18.3) (2022-06-01)


### Bug Fixes

* Fix test utils. ([55f7eb9](https://gitlab.com/jmorecroft/pg-stream/commit/55f7eb9a6f6d00bb8f6ab49452389583536510ed))





## [0.18.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.1...v0.18.2) (2022-06-01)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





## [0.18.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.0...v0.18.1) (2022-06-01)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





# [0.18.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.17.1...v0.18.0) (2022-05-24)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





## [0.17.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.17.0...v0.17.1) (2022-05-23)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





# [0.17.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.16.1...v0.17.0) (2022-05-23)


### Features

* Added options to GCP handler (and corresponding CLI) to set custom attributes and ordering key suffix fields. ([58bab4e](https://gitlab.com/jmorecroft/pg-stream/commit/58bab4e603df197380c6805043c437eb022dc76b))





## [0.16.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.16.0...v0.16.1) (2022-05-21)


### Bug Fixes

* **cli:** Fixed retry error check. ([91ee225](https://gitlab.com/jmorecroft/pg-stream/commit/91ee2252b5c65f2c05d32a548f58a9b310edc2c1))





# [0.16.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.15.0...v0.16.0) (2022-05-21)


### Features

* **cli:** Added optional retry to recvlogical and recvlogical-gcp commands. ([69a8218](https://gitlab.com/jmorecroft/pg-stream/commit/69a821892649587fc5d7e6eec48eafe2e842d528))





# [0.15.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.14.2...v0.15.0) (2022-05-20)


* feat(core)!: Simplified query function by removing decode, added new convenience function queryAndDecode for doing query and additional decode step. ([2e89eb2](https://gitlab.com/jmorecroft/pg-stream/commit/2e89eb24942e81ae34306f188880950103297185))


### BREAKING CHANGES

* query function no longer takes decoder input, must use queryAndDecode instead.





## [0.14.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.14.1...v0.14.2) (2022-05-19)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





# [0.14.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.13.3...v0.14.0) (2022-05-19)


* feat!: Added new package gcp containing a handler for pushing to GCP PubSub. ([29555b4](https://gitlab.com/jmorecroft/pg-stream/commit/29555b434bcd4c21b8e9a57d28b4f1834bb23bf9))


### BREAKING CHANGES

* Changed handler interface to allow passing in context (currently just contains a logger).





## [0.13.3](https://gitlab.com/jmorecroft/pg-stream/compare/v0.13.2...v0.13.3) (2022-05-16)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





## [0.13.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.13.1...v0.13.2) (2022-05-14)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





## [0.13.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.13.0...v0.13.1) (2022-05-14)


### Bug Fixes

* Core README updated. ([6de5a7b](https://gitlab.com/jmorecroft/pg-stream/commit/6de5a7bd583f36678441e7f18a22b3d1ff9a7d6f))





# [0.13.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.12.2...v0.13.0) (2022-05-14)


### Features

* Removed oclif, re-instated commander. ([8d57f1c](https://gitlab.com/jmorecroft/pg-stream/commit/8d57f1cdb86e312fc90267e57c978bca7b4b99e0))





## [0.12.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.12.1...v0.12.2) (2022-05-13)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





## [0.12.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.12.0...v0.12.1) (2022-05-13)

**Note:** Version bump only for package @jmorecroft67/pg-stream-cli





# 0.12.0 (2022-05-12)


### Bug Fixes

* Added "lerna link" at start of CI jobs. ([16fe422](https://gitlab.com/jmorecroft/pg-stream/commit/16fe422412fc39615acd7ac7bc60dac31346850a))
* GitLab CI config rework. ([b474fa2](https://gitlab.com/jmorecroft/pg-stream/commit/b474fa2e81313a11391bac336bc6927b252cdeb2))
* GitLab CI config tweaking. ([0e6fa5e](https://gitlab.com/jmorecroft/pg-stream/commit/0e6fa5ea64a052bfb91466fbe8cae748e4b7e81d))
* Remove before_script from child GitLab CI configs. ([dda7ce6](https://gitlab.com/jmorecroft/pg-stream/commit/dda7ce666f04ad0698b4ca1f8591b2f76caf56e4))


### Features

* GitLab CI config, missing file. ([d8507b2](https://gitlab.com/jmorecroft/pg-stream/commit/d8507b2e55bdb41d719a71fb870ba1a734277055))
