import 'jest-extended';
import { promisify } from 'util';
import { clearDb, cli } from 'test-utils';
import { PubSub, Topic, Subscription } from '@google-cloud/pubsub';

describe(__filename, () => {
  let pubsub: PubSub;
  let topic: Topic;
  let sub: Subscription;

  beforeAll(() => {
    const projectId = process.env['PUBSUB_PROJECT_ID'];
    if (!projectId) {
      fail();
    }
    pubsub = new PubSub({ projectId });
  });

  beforeEach(async () => {
    [topic] = await pubsub.createTopic({ name: 'test-cli-gcp-topic' });
    [sub] = await topic.createSubscription('test-cli-gcp-desc', {
      enableMessageOrdering: true
    });
  });

  afterEach(clearDb);
  afterEach(async () => {
    await sub?.delete();
    await topic?.delete();
  });

  it('should run recvlogical and exit with 0', async () => {
    await cli(
      'query',
      '-c',
      'CREATE TABLE test1_cli (id INTEGER, greeting VARCHAR)',
      '-c',
      'CREATE TABLE test2_cli (id INTEGER, greeting VARCHAR)',
      '-c',
      'CREATE TABLE test3_cli (id INTEGER, greeting VARCHAR)'
    );

    const received: unknown[] = [];
    sub.on('message', (message) => {
      received.push(JSON.parse(message.data.toString()));
    });

    const promise = cli(
      'recvlogical-gcp',
      '-S',
      'test_cli_slot',
      '-P',
      'test_cli_pub',
      '--createSlotIfNone',
      '--createPublicationIfNone',
      '-I',
      'test1_cli',
      '-I',
      'test2_cli',
      '-t',
      'test-cli-gcp-topic',
      '-q'
    );

    await promisify(setTimeout)(1000);

    await cli(
      'query',
      '-c',
      `INSERT INTO test1_cli VALUES (1, 'hello')`,
      '-c',
      `INSERT INTO test2_cli VALUES (1, 'hi')`,
      '-c',
      `INSERT INTO test3_cli VALUES (1, 'yo')`
    );

    await promisify(setTimeout)(1000);

    promise.child.kill('SIGINT');

    await promise;

    expect(promise.child.exitCode).toEqual(0);

    expect(received).toIncludeSameMembers([
      expect.objectContaining({
        type: 'Relation',
        name: 'test1_cli',
        namespace: 'public',
        columns: [
          expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
          expect.objectContaining({
            name: 'greeting',
            dataTypeName: 'varchar'
          })
        ]
      }),
      expect.objectContaining({
        type: 'Insert',
        namespace: 'public',
        name: 'test1_cli',
        newRecord: {
          id: 1,
          greeting: 'hello'
        }
      }),
      expect.objectContaining({
        type: 'Relation',
        name: 'test2_cli',
        namespace: 'public',
        columns: [
          expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
          expect.objectContaining({
            name: 'greeting',
            dataTypeName: 'varchar'
          })
        ]
      }),
      expect.objectContaining({
        type: 'Insert',
        namespace: 'public',
        name: 'test2_cli',
        newRecord: {
          id: 1,
          greeting: 'hi'
        }
      })
    ]);
  });
});
