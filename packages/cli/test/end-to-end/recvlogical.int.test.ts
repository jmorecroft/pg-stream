import 'jest-extended';
import { promisify } from 'util';
import { cli, parseJsonOutput, clearDb } from 'test-utils';

describe(__filename, () => {
  afterEach(clearDb);

  it('should run recvlogical and exit with 0', async () => {
    await cli(
      'query',
      '-c',
      'CREATE TABLE test1_cli (id INTEGER, greeting VARCHAR)',
      '-c',
      'CREATE TABLE test2_cli (id INTEGER, greeting VARCHAR)',
      '-c',
      'CREATE TABLE test3_cli (id INTEGER, greeting VARCHAR)'
    );

    const promise = cli(
      'recvlogical',
      '-S',
      'test_cli_slot',
      '-P',
      'test_cli_pub',
      '--createSlotIfNone',
      '--createPublicationIfNone',
      '-I',
      'test1_cli',
      '-I',
      'test2_cli',
      '-q'
    );

    await promisify(setTimeout)(500);

    await cli(
      'query',
      '-c',
      `INSERT INTO test1_cli VALUES (1, 'hello')`,
      '-c',
      `INSERT INTO test2_cli VALUES (1, 'hi')`,
      '-c',
      `INSERT INTO test3_cli VALUES (1, 'yo')`
    );

    promise.child.kill('SIGINT');

    const { stdout } = await promise;

    expect(promise.child.exitCode).toEqual(0);

    expect(
      parseJsonOutput(stdout).filter(
        (msg) => msg.type !== 'Begin' && msg.type !== 'Commit'
      )
    ).toEqual([
      expect.objectContaining({
        type: 'Relation',
        name: 'test1_cli',
        namespace: 'public',
        columns: [
          expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
          expect.objectContaining({
            name: 'greeting',
            dataTypeName: 'varchar'
          })
        ]
      }),
      expect.objectContaining({
        type: 'Insert',
        namespace: 'public',
        name: 'test1_cli',
        newRecord: {
          id: 1,
          greeting: 'hello'
        }
      }),
      expect.objectContaining({
        type: 'Relation',
        name: 'test2_cli',
        namespace: 'public',
        columns: [
          expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
          expect.objectContaining({
            name: 'greeting',
            dataTypeName: 'varchar'
          })
        ]
      }),
      expect.objectContaining({
        type: 'Insert',
        namespace: 'public',
        name: 'test2_cli',
        newRecord: {
          id: 1,
          greeting: 'hi'
        }
      })
    ]);
  });
});
