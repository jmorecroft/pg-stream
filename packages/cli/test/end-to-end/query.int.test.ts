import 'jest-extended';
import { cli, parseJsonOutput } from 'test-utils';

describe(__filename, () => {
  it('should run query and exit with 0', async () => {
    const promise = cli('query', '-c', 'select 1', '-c', 'select 2');

    const { child } = promise;
    const { stdout } = await promise;

    expect(child.exitCode).toEqual(0);

    const logs = parseJsonOutput(stdout);

    expect(logs).toContainValues([
      expect.objectContaining({
        result: JSON.stringify([
          {
            '?column?': 1
          }
        ])
      }),
      expect.objectContaining({
        result: JSON.stringify([
          {
            '?column?': 2
          }
        ])
      })
    ]);
  });
});
