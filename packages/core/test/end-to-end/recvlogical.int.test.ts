import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as T from 'fp-ts/Task';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as pg from '../../src';
import { fileSync, FileResult } from 'tmp';
import { readFileSync } from 'fs';
import { getOptionsFromEnv, clearDb, logger } from 'test-utils';
import { fail } from 'assert';

describe(__filename, () => {
  let tmpfile: FileResult;
  let options!: pg.RecvlogicalOptions;

  beforeEach(() => {
    tmpfile = fileSync();
  });
  beforeEach(() => {
    const env = getOptionsFromEnv();

    if (E.isLeft(env)) {
      fail(env.left);
    }

    options = {
      ...env.right,
      slotName: 'test_core_slot',
      publicationName: 'test_core_pub',
      createPublicationIfNone: true,
      createSlotIfNone: true,
      xlogs: {
        handler: pg.fileHandler(tmpfile.name),
        concurrency: 1
      },
      includeTables: ['test_core'],
      endAfter: {
        duration: 3000
      }
    };
  });

  afterEach(() => {
    tmpfile.removeCallback();
  });
  afterEach(clearDb);

  it('should sync table insert', (done) => {
    const createTable = pg.query(
      `CREATE TABLE test_core (id INTEGER PRIMARY KEY, word VARCHAR NOT NULL, flag BOOLEAN, matrix INT[][], jason JSON)`
    );

    const populateTable = TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, false),
            RTE.chain(() =>
              pg.query(`ALTER TABLE test_core REPLICA IDENTITY FULL`)
            ),
            RTE.chain(() =>
              pg.query(
                `INSERT INTO test_core VALUES 
                   (1, 'hello', null, null, '{"meaning":[42]}'), 
                   (2, 'gday', true, array[array[1,2,3], array[4,5,6]], null)`
              )
            ),
            RTE.chain(() =>
              pg.query(`UPDATE test_core SET word = 'hiya' WHERE id = 1`)
            ),
            RTE.chain(() => pg.query(`DELETE FROM test_core WHERE id = 1`))
          )
        ),
      (conn) => pg.close({ conn, logger })
    );

    TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, true),
            RTE.chain(() => createTable),
            RTE.chain(() =>
              RTE.sequenceArray([
                pg.recvlogical(options),
                RTE.fromTaskEither(
                  pipe(
                    T.delay(1000)(T.of(E.of(undefined))),
                    TE.chain(() => populateTable),
                    TE.map(() => undefined)
                  )
                )
              ])
            )
          )
        ),
      (conn) => pg.close({ conn, logger })
    )().then((result) => {
      pipe(
        result,
        E.fold(
          (error) => {
            done(error);
          },
          () => {
            const received = readFileSync(tmpfile.name)
              .toString('utf-8')
              .split('\n')
              .flatMap((line) => {
                if (line.trim().length === 0) {
                  return [];
                }
                return JSON.parse(line);
              })
              .filter((msg) => msg.type !== 'Begin' && msg.type !== 'Commit');

            expect(received).toEqual([
              expect.objectContaining({
                type: 'Relation',
                name: 'test_core',
                namespace: 'public',
                columns: [
                  expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
                  expect.objectContaining({
                    name: 'word',
                    dataTypeName: 'varchar'
                  }),
                  expect.objectContaining({
                    name: 'flag',
                    dataTypeName: 'bool'
                  }),
                  expect.objectContaining({
                    name: 'matrix',
                    dataTypeName: 'int4[]'
                  }),
                  expect.objectContaining({
                    name: 'jason',
                    dataTypeName: 'json'
                  })
                ]
              }),
              expect.objectContaining({
                type: 'Insert',
                namespace: 'public',
                name: 'test_core',
                newRecord: {
                  id: 1,
                  word: 'hello',
                  flag: null,
                  matrix: null,
                  jason: {
                    meaning: [42]
                  }
                }
              }),
              expect.objectContaining({
                type: 'Insert',
                namespace: 'public',
                name: 'test_core',
                newRecord: {
                  id: 2,
                  word: 'gday',
                  flag: true,
                  matrix: [
                    [1, 2, 3],
                    [4, 5, 6]
                  ],
                  jason: null
                }
              }),
              expect.objectContaining({
                type: 'Update',
                namespace: 'public',
                name: 'test_core',
                newRecord: {
                  id: 1,
                  word: 'hiya',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                },
                oldRecord: {
                  id: 1,
                  word: 'hello',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                }
              }),
              expect.objectContaining({
                namespace: 'public',
                name: 'test_core',
                type: 'Delete',
                oldRecord: {
                  id: 1,
                  word: 'hiya',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                }
              })
            ]);

            done();
          }
        )
      );
    });
  }, 10000);
});
