import * as O from 'fp-ts/Option';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import * as B from './parser/buffer';
import { pipe } from 'fp-ts/lib/function';
import { recvlogical } from './recvlogical';
import { open } from './open';
import { close } from './close';
import { RecvlogicalOptions } from './types';
import {
  PgClientMessageTypes,
  PgOutputMessageTypes,
  CopyData,
  pgClientMessageParser,
  makePgReadyForQuery,
  makePgRowDescription,
  makePgDataRow,
  makePgCopyBothResponse,
  makePgCopyData,
  PgTypes
} from './pg-protocol';
import { makeTestServer, TestServer, logger } from 'test-utils';
import 'jest-extended';

describe(__filename, () => {
  let server: TestServer<RecvlogicalOptions>;

  const handler = jest.fn(() => () => async () => E.right(undefined));

  const options: RecvlogicalOptions = {
    host: 'localhost',
    port: 12345,
    password: 'password',
    username: 'username',
    database: 'database',
    slotName: 'test',
    publicationName: 'test',
    heartbeatInterval: 100,
    xlogs: {
      handler
    }
  };

  beforeEach(() => {
    server = makeTestServer(options);
  });

  afterEach((done) => {
    jest.resetAllMocks();
    server.close(done);
  });

  const makeCopyData: (
    payload: PgOutputMessageTypes,
    walStart: bigint
  ) => CopyData = (payload, walStart) => ({
    type: 'CopyData',
    payload: {
      type: 'XLogData',
      payload,
      walStart,
      walEnd: 2n,
      timeStamp: 3n
    }
  });

  it('should receive from existing slot', (done) => {
    const received: PgClientMessageTypes[] = [];

    const queries = [
      {
        sql: "SELECT confirmed_flush_lsn FROM pg_replication_slots WHERE slot_name = 'test'",
        type: 'Query'
      },
      {
        sql: "SELECT * FROM pg_publication WHERE pubname = 'test'",
        type: 'Query'
      },
      {
        sql: "START_REPLICATION SLOT test LOGICAL A/A (proto_version '1', publication_names 'test')",
        type: 'Query'
      }
    ];

    const replies = [
      Buffer.concat([
        makePgRowDescription({
          fields: [
            {
              name: 'confirmed_flush_lsn',
              columnId: 1,
              dataTypeId: PgTypes.varchar.baseTypeOid,
              dataTypeModifier: 3,
              dataTypeSize: 4,
              format: 5,
              tableId: 6
            }
          ]
        }),
        makePgDataRow({
          values: ['A/A']
        }),
        makePgReadyForQuery({
          transactionStatus: 'T'
        })
      ]),
      Buffer.concat([
        makePgRowDescription({
          fields: [
            {
              name: 'any',
              columnId: 1,
              dataTypeId: 2,
              dataTypeModifier: 3,
              dataTypeSize: 4,
              format: 5,
              tableId: 6
            }
          ]
        }),
        makePgDataRow({
          values: ['blah']
        }),
        makePgReadyForQuery({
          transactionStatus: 'T'
        })
      ]),
      Buffer.concat([
        makePgCopyBothResponse({ binary: false, columnBinary: [] }),
        makePgCopyData(
          makeCopyData(
            {
              type: 'Relation',
              id: 123,
              namespace: 'public',
              name: 'test',
              replIdent: 'f',
              columns: [
                {
                  name: 'greeting',
                  dataTypeId: PgTypes.varchar.baseTypeOid,
                  isKey: true,
                  typeMod: -1
                },
                {
                  name: 'binaryData',
                  dataTypeId: PgTypes.bytea.baseTypeOid,
                  isKey: false,
                  typeMod: -1
                },
                {
                  name: 'createdAt',
                  dataTypeId: PgTypes.timestamptz.baseTypeOid,
                  isKey: false,
                  typeMod: -1
                },
                {
                  name: 'updatedAt',
                  dataTypeId: PgTypes.timestamptz.baseTypeOid,
                  isKey: false,
                  typeMod: -1
                }
              ]
            },
            0xa0000000bn
          )
        ),
        makePgCopyData(
          makeCopyData(
            {
              type: 'Insert',
              relationId: 123,
              newRecord: [
                O.some('hello'),
                O.some(Buffer.from([1, 2, 3])),
                O.some(null),
                O.none
              ]
            },
            0xa0000000cn
          )
        )
      ])
    ];

    server.negotiateSSL((socket) => {
      socket.on('data', (buf) => {
        pipe(
          pgClientMessageParser(B.stream(buf)),
          E.fold(
            (err) => {
              done(err);
            },
            ({ value }) => {
              received.push(value);

              const reply = replies[received.length - 1];
              if (reply) {
                socket.write(reply);
              }

              const compareLength = Math.min(received.length, queries.length);

              expect(received.slice(0, compareLength)).toEqual(
                queries.slice(0, compareLength)
              );

              const updates = received.slice(queries.length);

              const lsns = updates.flatMap(
                (update) =>
                  (update.type === 'CopyData' &&
                    update.payload.type === 'XStatusUpdate' &&
                    update.payload.lastWalWrite) ||
                  []
              );

              expect(lsns.length).toEqual(updates.length);

              if (lsns.length === 3) {
                expect(lsns).toBeOneOf([
                  [0xa0000000an, 0xa0000000cn, 0xa0000000cn],
                  [0xa0000000an, 0xa0000000bn, 0xa0000000cn]
                ]);
                socket.end();
              }
            }
          )
        );
      });
    });

    server.listen((options) => {
      TE.bracket(
        open({ options, logger }),
        (conn) => recvlogical(options)({ conn, logger }),
        (conn) => close({ conn, logger })
      )().then((result) => {
        pipe(
          result,
          E.fold(
            (err) => done(err),
            () => {
              expect(handler).toHaveBeenCalledTimes(2);
              expect(handler).toHaveBeenNthCalledWith(1, [
                {
                  walPos: 0xa0000000bn,
                  xlog: expect.objectContaining({
                    type: 'Relation',
                    namespace: 'public',
                    name: 'test',
                    columns: [
                      expect.objectContaining({
                        name: 'greeting',
                        dataTypeName: 'varchar',
                        isKey: true
                      }),
                      expect.objectContaining({
                        name: 'binaryData',
                        dataTypeName: 'bytea',
                        isKey: false
                      }),
                      expect.objectContaining({
                        name: 'createdAt',
                        dataTypeName: 'timestamptz',
                        isKey: false
                      }),
                      expect.objectContaining({
                        name: 'updatedAt',
                        dataTypeName: 'timestamptz',
                        isKey: false
                      })
                    ]
                  })
                }
              ]);
              expect(handler).toHaveBeenNthCalledWith(2, [
                {
                  walPos: 0xa0000000cn,
                  xlog: {
                    type: 'Insert',
                    namespace: 'public',
                    name: 'test',
                    newRecord: {
                      greeting: 'hello',
                      binaryData: Buffer.from([1, 2, 3]),
                      createdAt: null
                    }
                  }
                }
              ]);
              done();
            }
          )
        );
      });
    });
  });
});
