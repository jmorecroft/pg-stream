import { pipe } from 'fp-ts/lib/function';
import { open } from './open';
import { close } from './close';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import { BaseOptions } from './types';
import { logger, makeTestServer, TestServer } from 'test-utils';

describe(__filename, () => {
  let server: TestServer<BaseOptions>;

  const options: BaseOptions = {
    host: 'localhost',
    port: 12345,
    password: 'password',
    username: 'username',
    database: 'database'
  };

  beforeEach(() => {
    server = makeTestServer(options);
  });

  afterEach((done) => {
    server.close(done);
  });

  it('should connect', (done) => {
    server.listen((options) => {
      server.negotiateSSL();

      pipe(
        open({ options, logger }),
        TE.chain((conn) => close({ conn, logger }))
      )().then((val) => {
        pipe(
          val,
          E.fold(
            (err) => {
              done(err);
            },
            () => {
              done();
            }
          )
        );
      });
    });
  });

  it('should fail to connect', (done) => {
    pipe(
      open({ options, logger }),
      TE.chain((conn) => close({ conn, logger }))
    )().then((val) => {
      pipe(
        val,
        E.fold(
          () => {
            done();
          },
          () => {
            fail();
          }
        )
      );
    });
  });
});
