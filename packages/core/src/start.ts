import {
  AuthenticationSASL,
  AuthenticationSASLContinue,
  AuthenticationSASLFinal,
  BackendKeyData,
  ParameterStatus,
  PasswordMessage,
  PgServerMessageTypes,
  ReadyForQuery,
  SASLInitialResponse,
  SASLResponse,
  StartupMessage,
  serverFinalMessageParser,
  serverFirstMessageParser
} from './pg-protocol';
import { BaseOptions, PgContext } from './types';
import {
  PgBackendError,
  PgParseError,
  PgQueryAbortedError,
  PgSaslError,
  PgUnexpectedMessagesError
} from './errors';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';
import * as S from 'parser-ts/string';
import { pipe } from 'fp-ts/function';
import { Transform } from 'stream';
import { createHash, randomBytes, createHmac, BinaryLike } from 'crypto';
import * as _ from 'lodash';
import { logBackendMessage } from './utils';

export type StartupResponse = BackendKeyData | ParameterStatus | ReadyForQuery;

export const start: (
  options: BaseOptions,
  replication?: boolean
) => RTE.ReaderTaskEither<PgContext, Error, StartupResponse[]> =
  ({ username, password, database }, replication) =>
  ({ conn, logger }) =>
  () => {
    const received: StartupResponse[] = [];

    return new Promise((resolve) => {
      const done = _.once((result: E.Either<Error, StartupResponse[]>) => {
        pipe(
          result,
          E.fold(
            (error) =>
              logger.error(error, 'Failed to complete startup sequence.'),
            () => logger.debug('Startup sequence completed.')
          )
        );
        resolve(result);
      });
      let saslState: SASLState = {
        type: 'SASLNextState',
        password
      };

      const processor = new Transform({
        objectMode: true,
        construct(callback) {
          const parameters = [
            {
              name: 'database',
              value: database
            },
            {
              name: 'user',
              value: username
            }
          ];
          if (replication) {
            parameters.push({
              name: 'replication',
              value: 'database'
            });
          }
          const startup: StartupMessage = {
            type: 'StartupMessage',
            protocolVersion: 196608,
            parameters
          };
          this.push(startup);
          callback();
        },
        transform(chunk: PgServerMessageTypes, encoding, callback) {
          switch (chunk.type) {
            case 'AuthenticationSASL': {
              if (saslState.type !== 'SASLNextState') {
                callback(new PgSaslError('Unexpected SASL state'));
                break;
              }
              pipe(
                handleSASL(chunk, saslState),
                E.fold(
                  (error) => callback(error),
                  ([nextState, result]) => {
                    saslState = nextState;
                    this.push(result);
                    callback();
                  }
                )
              );
              break;
            }
            case 'AuthenticationSASLContinue': {
              if (saslState.type !== 'SASLContinueNextState') {
                callback(new PgSaslError('Unexpected SASL state'));
                break;
              }
              pipe(
                handleSASLContinue(chunk, saslState),
                E.fold(
                  (error) => callback(error),
                  ([nextState, result]) => {
                    saslState = nextState;
                    this.push(result);
                    callback();
                  }
                )
              );
              break;
            }
            case 'AuthenticationSASLFinal': {
              if (saslState.type !== 'SASLFinalNextState') {
                callback(new PgSaslError('Unexpected SASL state'));
                break;
              }
              pipe(
                handleSASLFinal(chunk, saslState),
                E.fold(
                  (error) => callback(error),
                  (nextState) => {
                    saslState = nextState;
                    callback();
                  }
                )
              );
              break;
            }
            case 'AuthenticationCleartextPassword': {
              const message: PasswordMessage = {
                type: 'PasswordMessage',
                password
              };
              this.push(message);
              callback();
              break;
            }
            case 'AuthenticationMD5Password': {
              const message: PasswordMessage = {
                type: 'PasswordMessage',
                password: createHash('md5')
                  .update(
                    createHash('md5')
                      .update(password.concat(username))
                      .digest('hex')
                      .concat(Buffer.from(chunk.salt).toString('hex'))
                  )
                  .digest('hex')
              };
              this.push(message);
              callback();
              break;
            }
            case 'ErrorResponse': {
              logBackendMessage(logger, chunk);
              callback(new PgBackendError([chunk]));
              break;
            }
            case 'AuthenticationOk': {
              callback();
              break;
            }
            case 'NoticeResponse': {
              logBackendMessage(logger, chunk);
              callback();
              break;
            }
            case 'BackendKeyData':
            case 'ParameterStatus': {
              received.push(chunk);
              callback();
              break;
            }
            case 'ReadyForQuery': {
              received.push(chunk);
              this.push(null);
              done(E.right(received));
              break;
            }
            default: {
              callback(new PgUnexpectedMessagesError([chunk]));
              break;
            }
          }
        }
      });

      processor.once('final', () => {
        done(E.left(new PgQueryAbortedError(received)));
        processor.destroy();
      });

      processor.once('error', (error) => {
        done(E.left(error));
        processor.destroy();
      });

      conn.processorWrite.write(processor);
    });
  };

type SASLNextState = { type: 'SASLNextState'; password: string };

type SASLContinueNextState = {
  type: 'SASLContinueNextState';
  clientNonce: string;
  clientFirstMessageHeader: string;
  clientFirstMessageBody: string;
  password: string;
};

type SASLFinalNextState = {
  type: 'SASLFinalNextState';
  serverSignature: Buffer;
};

type SASLCompleteState = { type: 'SASLCompleteState' };

type SASLState =
  | SASLNextState
  | SASLContinueNextState
  | SASLFinalNextState
  | SASLCompleteState;

const handleSASL: (
  message: AuthenticationSASL,
  state: SASLNextState
) => E.Either<Error, [SASLContinueNextState, SASLInitialResponse]> = (
  message,
  state
) => {
  const mechanism = message.mechanisms.find((item) => item === 'SCRAM-SHA-256');
  if (!mechanism) {
    return E.left(new PgSaslError('SCRAM-SHA-256 not a supported mechanism'));
  }

  const { password } = state;

  const clientNonce = randomBytes(18).toString('base64');
  const clientFirstMessageHeader = 'n,,';
  const clientFirstMessageBody = `n=*,r=${clientNonce}`;
  const clientFirstMessage = `${clientFirstMessageHeader}${clientFirstMessageBody}`;
  const nextState: SASLState = {
    type: 'SASLContinueNextState',
    clientNonce,
    clientFirstMessageHeader,
    clientFirstMessageBody,
    password
  };
  const result: SASLInitialResponse = {
    type: 'SASLInitialResponse',
    mechanism,
    clientFirstMessage
  };

  return E.right([nextState, result]);
};

const handleSASLContinue: (
  message: AuthenticationSASLContinue,
  state: SASLContinueNextState
) => E.Either<Error, [SASLFinalNextState, SASLResponse]> = (message, state) => {
  const { serverFirstMessage } = message;
  const {
    clientFirstMessageBody,
    clientFirstMessageHeader,
    clientNonce,
    password
  } = state;
  return pipe(
    S.run(serverFirstMessage)(serverFirstMessageParser(clientNonce)),
    E.mapLeft((error) => new PgParseError(error)),
    E.fold(
      (error) => E.left(error),
      ({ value: { serverNonce, salt, iterationCount } }) => {
        // Credit to Brian C (pg author) for much of the below crypto finagling, which I
        // shamelessly stole and tweaked here and there!

        const saltedPassword = Hi(password, salt, iterationCount);
        const clientKey = hmacSha256(saltedPassword, 'Client Key');
        const storedKey = sha256(clientKey);
        const clientFinalMessageWithoutProof = `c=${Buffer.from(
          clientFirstMessageHeader
        ).toString('base64')},r=${clientNonce}${serverNonce}`;
        const authMessage = `${clientFirstMessageBody},${serverFirstMessage},${clientFinalMessageWithoutProof}`;

        const clientSignature = hmacSha256(storedKey, authMessage);
        const clientProofBytes = xorBuffers(clientKey, clientSignature);
        const clientProof = clientProofBytes.toString('base64');

        const serverKey = hmacSha256(saltedPassword, 'Server Key');
        const serverSignature = hmacSha256(serverKey, authMessage);
        const clientFinalMessage =
          clientFinalMessageWithoutProof + ',p=' + clientProof;

        const nextState: SASLState = {
          type: 'SASLFinalNextState',
          serverSignature
        };
        const result: SASLResponse = {
          type: 'SASLResponse',
          clientFinalMessage
        };
        return E.right([nextState, result]);
      }
    )
  );
};

const handleSASLFinal: (
  message: AuthenticationSASLFinal,
  state: SASLFinalNextState
) => E.Either<Error, SASLCompleteState> = (message, state) => {
  const { serverFinalMessage } = message;
  const { serverSignature } = state;
  return pipe(
    S.run(serverFinalMessage)(serverFinalMessageParser),
    E.mapLeft((error): Error => new PgParseError(error)),
    E.chain((result) => {
      if (result.value.serverSignature.compare(serverSignature) === 0) {
        return E.right(undefined);
      }
      return E.left(
        new PgSaslError('Received server signature does not match expected')
      );
    }),
    E.fold(
      (error) => E.left(error),
      () => {
        const nextState: SASLState = {
          type: 'SASLCompleteState'
        };
        return E.right(nextState);
      }
    )
  );
};

const sha256 = (text: BinaryLike) => {
  return createHash('sha256').update(text).digest();
};

const hmacSha256 = (key: BinaryLike, msg: BinaryLike) => {
  return createHmac('sha256', key).update(msg).digest();
};

const xorBuffers = (a: Buffer, b: Buffer) =>
  Buffer.from(a.map((_, i) => a[i] ^ b[i]));

const Hi = (password: string, saltBytes: Buffer, iterations: number) => {
  let ui1 = hmacSha256(
    password,
    Buffer.concat([saltBytes, Buffer.from([0, 0, 0, 1])])
  );

  let ui = ui1;
  _.times(iterations - 1, () => {
    ui1 = hmacSha256(password, ui1);
    ui = xorBuffers(ui, ui1);
  });

  return ui;
};
