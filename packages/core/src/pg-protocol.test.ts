import {
  BackendKeyData,
  CopyData,
  DataRow,
  ErrorResponse,
  NoticeResponse,
  ParameterStatus,
  pgServerMessageParser,
  ReadyForQuery,
  RowDescription,
  makePgAuthenticateOk,
  makePgBackendKeyData,
  makePgCopyData,
  makePgDataRow,
  makePgErrorResponse,
  makePgNoticeResponse,
  makePgParameterStatus,
  makePgReadyForQuery,
  makePgRowDescription
} from './pg-protocol';
import * as B from './parser/buffer';
import * as PR from 'parser-ts/ParseResult';
import * as E from 'fp-ts/Either';
import * as O from 'fp-ts/Option';

describe(__filename, () => {
  describe('AuthenticationOk', () => {
    it('should parse', () => {
      const buf = makePgAuthenticateOk();

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(
          { type: 'AuthenticationOk' },
          B.stream(buf, buf.length),
          start
        )
      );
    });
  });

  describe('BackendKeyData', () => {
    it('should parse', () => {
      const message: BackendKeyData = {
        type: 'BackendKeyData',
        pid: 1234,
        secretKey: 5678
      };

      const buf = makePgBackendKeyData(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, buf.length), start)
      );
    });
  });

  describe('ReadyForQuery', () => {
    it('should parse', () => {
      const message: ReadyForQuery = {
        type: 'ReadyForQuery',
        transactionStatus: 'I'
      };
      const buf = makePgReadyForQuery(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, buf.length), start)
      );
    });

    it('should fail to parse with bad txn status', () => {
      const message: ReadyForQuery = {
        type: 'ReadyForQuery',
        transactionStatus: 'X' as any
      };
      const buf = makePgReadyForQuery(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        E.left({
          input: B.stream(buf, 5),
          expected: [expect.stringMatching(/Invalid value "X"/)],
          fatal: true
        })
      );
    });
  });

  describe('ParameterStatus', () => {
    it('should parse', () => {
      const message: ParameterStatus = {
        type: 'ParameterStatus',
        name: 'param',
        value: 'value'
      };
      const buf = makePgParameterStatus(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, buf.length), start)
      );
    });

    it('should fail to parse with bad length', () => {
      const message: ParameterStatus = {
        type: 'ParameterStatus',
        name: 'param',
        value: 'value'
      };
      const buf = makePgParameterStatus(message);
      buf.writeInt32BE(10, 1); // overwrite

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        E.left({
          input: B.stream(buf, 5),
          expected: [expect.stringMatching(/Consumed 12 .*, expected 6/)],
          fatal: true
        })
      );
    });
  });

  describe('ErrorResponse', () => {
    it('should parse', () => {
      const message: ErrorResponse = {
        type: 'ErrorResponse',
        errors: [
          {
            type: 'X',
            value: 'error1'
          },
          {
            type: 'Y',
            value: 'error2'
          }
        ]
      };
      const buf = makePgErrorResponse(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, 22), start)
      );
    });

    it('should fail to parse with zero errors', () => {
      const message: ErrorResponse = {
        type: 'ErrorResponse',
        errors: [] as any
      };
      const buf = makePgErrorResponse(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        E.left({
          input: B.stream(buf, 6),
          fatal: true,
          expected: []
        })
      );
    });

    it('should fail to parse without final delimeter', () => {
      const message: ErrorResponse = {
        type: 'ErrorResponse',
        errors: [
          {
            type: 'X',
            value: 'error1'
          },
          {
            type: 'Y',
            value: 'error2'
          }
        ]
      };
      let buf = makePgErrorResponse(message);
      buf = Buffer.concat([buf], buf.length - 1);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        E.left({
          input: B.stream(buf, 5),
          fatal: false,
          expected: [expect.stringMatching(/Expected >= .* item\(s\)/)]
        })
      );
    });
  });

  describe('NoticeResponse', () => {
    it('should parse', () => {
      const message: NoticeResponse = {
        type: 'NoticeResponse',
        notices: [
          {
            type: 'X',
            value: 'notice1'
          },
          {
            type: 'Y',
            value: 'notice2'
          }
        ]
      };
      const buf = makePgNoticeResponse(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, 24), start)
      );
    });
  });

  describe('RowDescription', () => {
    it('should parse', () => {
      const message: RowDescription = {
        type: 'RowDescription',
        fields: [
          {
            name: 'col1',
            tableId: 1,
            columnId: 2,
            dataTypeId: 3,
            dataTypeModifier: 5,
            dataTypeSize: 4,
            format: 6
          },
          {
            name: 'col2',
            tableId: 7,
            columnId: 8,
            dataTypeId: 9,
            dataTypeSize: 10,
            dataTypeModifier: 11,
            format: 12
          }
        ]
      };
      const buf = makePgRowDescription(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, buf.length), start)
      );
    });
  });

  describe('DataRow', () => {
    it('should parse', () => {
      const message: DataRow = {
        type: 'DataRow',
        values: ['value1', 'value2', null]
      };
      const buf = makePgDataRow(message);

      const start = B.stream(buf);

      expect(pgServerMessageParser(start)).toEqual(
        PR.success(message, B.stream(buf, buf.length), start)
      );
    });

    describe('CopyData', () => {
      it('should parse xlogdata', () => {
        const message: CopyData = {
          type: 'CopyData',
          payload: {
            type: 'XLogData',
            walStart: 1n,
            walEnd: 2n,
            timeStamp: 3n,
            payload: {
              type: 'Insert',
              relationId: 123,
              newRecord: [
                O.some(null),
                O.some('hello'),
                O.some(Buffer.from([1, 2, 3])),
                O.none
              ]
            }
          }
        };
        const buf = makePgCopyData(message);

        const start = B.stream(buf);

        expect(pgServerMessageParser(start)).toEqual(
          PR.success(message, B.stream(buf, buf.length), start)
        );
      });

      it('should parse xkeepalive', () => {
        const message: CopyData = {
          type: 'CopyData',
          payload: {
            type: 'XKeepAlive',
            walEnd: 1n,
            timeStamp: 2n,
            replyNow: true
          }
        };
        const buf = makePgCopyData(message);

        const start = B.stream(buf);

        expect(pgServerMessageParser(start)).toEqual(
          PR.success(message, B.stream(buf, buf.length), start)
        );
      });
    });
  });
});
