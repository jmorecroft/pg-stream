import {
  DataRow,
  ErrorResponse,
  PgServerMessageTypes,
  Query,
  RowDescription,
  makeValueTypeParser,
  MakeValueTypeParserOptions
} from './pg-protocol';
import { PgContext } from './types';
import {
  PgBackendError,
  PgQueryAbortedError,
  PgQueryNoResultsError,
  PgRowValidationError,
  PgUnexpectedMessagesError
} from './errors';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';
import * as S from 'parser-ts/string';
import { Transform } from 'stream';
import { pipe } from 'fp-ts/lib/function';
import * as _ from 'lodash';
import * as RNEA from 'fp-ts/lib/ReadonlyArray';
import { logBackendMessage } from './utils';
import { stringify } from './helpers';
import * as t from 'io-ts';

type ResultSet = Record<string, unknown>[];

export const queryAndDecode = <A>(
  sql: string,
  decode: t.Decoder<unknown, A>,
  options?: MakeValueTypeParserOptions
) =>
  pipe(
    query(sql, options),
    RTE.chain((rs) =>
      RTE.fromEither(
        pipe(
          decode.decode(rs),
          E.mapLeft((e): Error => new PgRowValidationError(e))
        )
      )
    )
  );

export const query: (
  sql: string,
  options?: MakeValueTypeParserOptions
) => RTE.ReaderTaskEither<PgContext, Error, ResultSet> =
  (sql, options) =>
  ({ conn: { processorWrite }, logger }) =>
  () => {
    const received: PgServerMessageTypes[] = [];

    return new Promise((resolve) => {
      const done = _.once((result: E.Either<Error, ResultSet>) => {
        pipe(
          result,
          E.fold(
            (err) => logger.error({ err, sql }, 'Query failed.'),
            (rows) =>
              logger.info({ result: stringify(rows) }, 'Query complete.')
          )
        );
        resolve(result);
      });

      const processor = new Transform({
        objectMode: true,
        construct(callback) {
          const query: Query = { type: 'Query', sql };
          logger.debug({ sql }, 'Sending SQL query.');
          this.push(query);
          callback();
        },
        transform(chunk: PgServerMessageTypes, encoding, callback) {
          received.push(chunk);

          if (chunk.type !== 'ReadyForQuery') {
            if (
              chunk.type === 'ErrorResponse' ||
              chunk.type === 'NoticeResponse'
            ) {
              logBackendMessage(logger, chunk);
            }
            if (chunk.type === 'CommandComplete') {
              logger.info({ tag: chunk.commandTag }, 'Command complete.');
            }
            callback();
            return;
          }

          if (!RNEA.isNonEmpty(received)) {
            callback(new PgQueryNoResultsError());
            return;
          }

          const errors = received.filter(
            (result): result is ErrorResponse => result.type === 'ErrorResponse'
          );
          if (RNEA.isNonEmpty(errors)) {
            callback(new PgBackendError(errors));
            return;
          }

          const rowDescription = received.find(
            (result): result is RowDescription =>
              result.type === 'RowDescription'
          );
          const dataRows = received.filter(
            (result): result is DataRow => result.type === 'DataRow'
          );
          let rows: ResultSet = [];
          if (rowDescription) {
            const parsers = rowDescription.fields.map(
              ({ name, dataTypeId }) => ({
                name,
                parser: makeValueTypeParser(dataTypeId, options)
              })
            );
            rows = dataRows.map((row) =>
              parsers.reduce((acc, { name, parser }, index) => {
                const input = row.values[index];
                if (input !== null) {
                  const parsed = pipe(
                    S.run(input)(parser),
                    E.fold(
                      ({ expected }) => {
                        logger.warn(
                          { name, expected, input },
                          'Failed to parse row value.'
                        );
                        // Just use the original input.
                        return input;
                      },
                      ({ value }) => value
                    )
                  );
                  return { ...acc, [name]: parsed };
                }
                return { ...acc, [name]: input };
              }, {})
            );
          } else if (rows.length > 0) {
            callback(new PgUnexpectedMessagesError(received));
            return;
          }
          this.push(null);
          callback();
          done(E.right(rows));
        }
      });

      processor.once('final', () => {
        done(E.left(new PgQueryAbortedError(received)));
        processor.destroy();
      });

      processor.once('error', (error) => {
        done(E.left(error));
        processor.destroy();
      });

      processorWrite.write(processor);
    });
  };
