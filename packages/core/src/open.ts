import { PgConnection, BaseOptions, Logger } from './types';
import { PgConnectError } from './errors';
import * as net from 'net';
import * as tls from 'tls';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';
import { makeEncode, makeDecode, makeProcessorHandler } from './streams';
import * as _ from 'lodash';
import { pipe } from 'fp-ts/lib/function';
import { makePgMessage } from './pg-protocol';

interface Env {
  options: BaseOptions;
  logger: Logger;
}

export const open: RTE.ReaderTaskEither<Env, Error, PgConnection> =
  ({ options: { host, port, useSSL }, logger }) =>
  () => {
    return new Promise((resolve) => {
      const done = _.once((result: E.Either<Error, PgConnection>) => {
        pipe(
          result,
          E.fold(
            (error) => logger.error(error, 'Failed to connect.'),
            () => logger.info({ host, port }, 'Connection established.')
          )
        );
        resolve(result);
      });

      const onConnect = (pgSocket: net.Socket) => {
        const pgRead = pgSocket.pipe(makeDecode());
        const pgWrite = makeEncode();
        pgWrite.pipe(pgSocket);

        const processorWrite = makeProcessorHandler({
          pgRead,
          pgWrite
        });

        done(
          E.right({
            pgSocket,
            pgRead,
            pgWrite,
            processorWrite
          })
        );
      };

      const socket = net.connect({ host, port }, () => {
        // If we explicitly don't want to use SSL, we're done.
        if (useSSL === false) {
          onConnect(socket);
          return;
        }

        socket.write(
          makePgMessage({ type: 'SSLRequest', requestCode: 80877103 }),
          (error) => {
            if (error) {
              done(E.left(new PgConnectError(error)));
              return;
            }

            socket.once('readable', () => {
              const sslReply = (socket.read() as Buffer).toString();

              switch (sslReply) {
                case 'S': {
                  // SSL is enabled. Upgrade our socket to SSL-capable.
                  const secureSocket: tls.TLSSocket = tls.connect(
                    { socket },
                    () => onConnect(secureSocket)
                  );
                  secureSocket.once('error', (error) => {
                    done(E.left(new PgConnectError(error)));
                  });
                  break;
                }
                case 'N': {
                  // SSL is not enabled. Do we care?
                  if (useSSL) {
                    // Yes.
                    socket.destroy(
                      new PgConnectError(
                        new Error('Postgres server is not enabled for SSL.')
                      )
                    );
                  } else {
                    logger.warn(
                      'SSL is not enabled on the Postgres server but connecting anyway.'
                    );
                    onConnect(socket);
                  }
                  break;
                }
                default: {
                  socket.destroy(
                    new PgConnectError(
                      new Error(`SSL request reply not expected: ${sslReply}`)
                    )
                  );
                }
              }
            });
          }
        );
      });

      socket.once('error', (error) => {
        done(E.left(new PgConnectError(error)));
      });
    });
  };
