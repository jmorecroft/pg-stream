import { Duplex, Readable, Writable } from 'stream';

export const makeProcessorHandler = ({
  pgRead,
  pgWrite
}: {
  pgRead: Readable;
  pgWrite: Writable;
}): Writable => {
  return new Writable({
    objectMode: true,
    write(
      processor: { readable: Readable; writable: Writable } | Duplex,
      encoding,
      callback
    ) {
      const { readable, writable } =
        processor instanceof Duplex
          ? { readable: processor, writable: processor }
          : processor;

      pgRead.pipe(writable);
      pgRead.once('error', (error) => writable.destroy(error));

      readable.pipe(pgWrite, { end: false });

      readable.once('end', () => {
        pgRead.unpipe(writable);
        callback();
      });
    }
  });
};
