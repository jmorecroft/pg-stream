import { Transform } from 'stream';
import { pgServerMessageParser } from '../pg-protocol';
import * as P from 'parser-ts/Parser';
import * as E from 'fp-ts/Either';
import * as B from '../parser/buffer';
import { pipe } from 'fp-ts/lib/function';
import { PgParseError } from '../errors';

export const makeDecode = (): Transform => {
  let unusedBuf = Buffer.from([]);

  return new Transform({
    readableObjectMode: true,
    writableObjectMode: false,
    transform(chunk: Buffer, encoding, callback) {
      const buf = Buffer.concat([unusedBuf, chunk]);
      pipe(
        P.many1(pgServerMessageParser)(B.stream(buf)),
        E.fold(
          (error) => {
            if (error.fatal) {
              callback(new PgParseError(error));
              return;
            }
            unusedBuf = buf;
            callback();
          },
          (result) => {
            result.value.forEach((msg) => this.push(msg));
            const { cursor } = result.next;
            unusedBuf = buf.slice(cursor);
            callback();
          }
        )
      );
    }
  });
};
