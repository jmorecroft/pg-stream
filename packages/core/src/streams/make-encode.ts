import { Transform } from 'stream';
import { makePgMessage, PgClientMessageTypes } from '../pg-protocol';

export const makeEncode = (): Transform => {
  return new Transform({
    readableObjectMode: false,
    writableObjectMode: true,
    transform(message: PgClientMessageTypes, encoding, callback) {
      this.push(makePgMessage(message));
      callback();
    }
  });
};
