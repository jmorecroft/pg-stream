import { PgContext } from './types';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';

export const close: RTE.ReaderTaskEither<PgContext, Error, void> =
  ({ conn: { pgSocket, pgRead, pgWrite, processorWrite }, logger }) =>
  () => {
    return new Promise((resolve) => {
      pgRead.destroy();
      pgWrite.destroy();
      pgSocket.destroy();
      processorWrite.destroy();
      logger.info('Connection closed.');
      resolve(E.right(undefined));
    });
  };
