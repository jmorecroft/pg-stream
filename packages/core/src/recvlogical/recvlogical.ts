import { PgContext, RecvlogicalOptions } from '../types';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { pipeline } from 'stream';
import { pipe } from 'fp-ts/lib/function';
import {
  makeWalPositionSender,
  makeHeartbeat,
  makeXlogHandler,
  makeCopyDataFilter,
  makeTypeDecorator
} from './streams';
import { PgStreamError } from '../errors';

export const recvlogical: (
  options: RecvlogicalOptions
) => RTE.ReaderTaskEither<PgContext, Error, void> = (options) =>
  pipe(
    makeWalPositionSender(options),
    RTE.bindTo('walPositionSender'),
    RTE.bind(
      'copyDataFilter',
      () =>
        ({ logger }) =>
          TE.of(makeCopyDataFilter(logger))
    ),
    RTE.bind(
      'xlogHandler',
      () =>
        ({ logger }) =>
          TE.of(makeXlogHandler(options)(logger))
    ),
    RTE.chain(({ copyDataFilter, walPositionSender, xlogHandler }) => {
      const { heartbeat, next } = makeHeartbeat(options);
      const typeDecorator = makeTypeDecorator();

      copyDataFilter.once('copyBothResponse', () => {
        heartbeat.pipe(walPositionSender);
      });

      copyDataFilter.on('keepAlive', (keepAlive) => {
        if (keepAlive.replyNow) {
          next.flush();
        }
      });

      const writable = copyDataFilter;

      process.once('SIGINT', () => {
        writable.end();
      });

      const { endAfter } = options;

      if (endAfter) {
        if ('duration' in endAfter) {
          setTimeout(() => {
            writable.end();
          }, endAfter.duration);
        }
      }

      return ({ conn, logger }) =>
        () =>
          new Promise<E.Either<Error, void>>((resolve) => {
            const readable = pipeline(
              writable,
              typeDecorator,
              xlogHandler,
              walPositionSender,
              (error) => {
                heartbeat.destroy();
                if (error) {
                  logger.error(error, 'Logical streaming failed.');
                  resolve(E.left(new PgStreamError(error)));
                  return;
                }
                logger.info('Logical streaming complete.');
                resolve(E.right(undefined));
              }
            );

            const processor = { readable, writable };

            conn.processorWrite.write(processor);
          });
    })
  );
