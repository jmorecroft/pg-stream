import * as E from 'fp-ts/Either';
import { Writable } from 'stream';
import { createWriteStream } from 'fs';
import { stringify } from '../../helpers';
import { XlogHandler } from '../../types';

// This handler will send to the stream and only signal done once the last
// log is fully flushed. We pair this with a file output stream in "as" mode
// and therefore can be confident that we only signal done, which ultimately
// will update the last WAL position, once all data is flushed to the file.
const handler: (stream: Writable) => XlogHandler =
  (stream) => (xlogs) => () => () =>
    new Promise<E.Either<Error, undefined>>((resolve) => {
      const write = (remaining: string[]) => {
        for (const [index, xlog] of remaining.entries()) {
          if (index === remaining.length - 1) {
            // Last log - wait for confirmation it's been fully flushed.
            // (and therefore all the logs before it)
            stream.write(xlog, (error) => {
              if (error) {
                resolve(E.left(error));
                return;
              }
              resolve(E.right(undefined));
            });
          } else {
            // Wait for drain event before continuing if buffer is full.
            if (!stream.write(xlog)) {
              stream.once('drain', () => {
                write(remaining.slice(index + 1));
              });
              return;
            }
          }
        }
      };

      write(xlogs.map(({ xlog }) => `${stringify(xlog)}\n`));
    });

export const fileHandler = (path: string) =>
  // as - appending in synchronous mode, so once we get a flush callback
  //      we can be confident the data is completely synced to the file.
  handler(createWriteStream(path, { flags: 'as' }));
