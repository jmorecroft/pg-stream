import { Writable } from 'stream';
import * as E from 'fp-ts/Either';
import { stderr, stdout } from 'process';
import { stringify } from '../../helpers';
import { XlogHandler } from '../../types';

// Send to the stream and respect buffers but don't wait till
// fully flushed - this is only meant for stdout/stderr after
// all.
const handler: (stream: Writable) => XlogHandler =
  (stream) => (xlogs) => () => async () => {
    for (const { xlog } of xlogs) {
      if (!stream.write(`${stringify(xlog)}\n`)) {
        await new Promise((resolve) => {
          stream.once('drain', () => {
            resolve(undefined);
          });
        });
      }
    }
    return E.right(undefined);
  };

export const stdoutHandler = handler(stdout);
export const stderrHandler = handler(stderr);
