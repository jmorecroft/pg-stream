import * as t from 'io-ts';
import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/lib/function';

export const walLsnFromString = new t.Type<bigint, string, unknown>(
  'WalLsnFromString',
  (u): u is bigint => typeof u === 'bigint',
  (u, c) =>
    pipe(
      t.string.validate(u, c),
      E.chain((s) => {
        const match = s.match(/^([A-F0-9]+)\/([A-F0-9]+)$/);
        if (match) {
          const top = BigInt(Number.parseInt(match[1], 16));
          const bottom = BigInt(Number.parseInt(match[2], 16));
          return t.success((top << 32n) + bottom);
        }
        return t.failure(u, c);
      })
    ),
  (a) =>
    `${Number(a >> 32n)
      .toString(16)
      .toUpperCase()}/${Number(a & 0xffffffffn)
      .toString(16)
      .toUpperCase()}`
);
