import { Transform } from 'stream';
import {
  Delete,
  Insert,
  Relation,
  Truncate,
  TupleData,
  Update,
  PgOutputMessageTypes,
  getTypeName,
  makeValueTypeParser,
  ValueType
} from '../../pg-protocol';
import * as P from 'parser-ts/Parser';
import * as S from 'parser-ts/string';
import { pipe } from 'fp-ts/function';
import * as O from 'fp-ts/Option';
import * as E from 'fp-ts/Either';
import { PgOutputStateError } from '../../errors';

export type DecoratedRelation = Omit<Relation, 'columns'> & {
  columns: (Relation['columns'][number] & {
    dataTypeName: ReturnType<typeof getTypeName>;
  })[];
};

export type DecoratedInsert = Omit<Insert, 'relationId' | 'newRecord'> & {
  namespace: string;
  name: string;
  newRecord: Record<string, ValueType | null>;
};

export type DecoratedUpdate = Omit<
  Update,
  'relationId' | 'newRecord' | 'oldRecord' | 'oldKey'
> & {
  namespace: string;
  name: string;
  oldKey?: Record<string, ValueType | null>;
  oldRecord?: Record<string, ValueType | null>;
  newRecord: Record<string, ValueType | null>;
};

export type DecoratedDelete = Omit<
  Delete,
  'relationId' | 'oldRecord' | 'oldKey'
> & {
  namespace: string;
  name: string;
  oldKey?: Record<string, ValueType | null>;
  oldRecord?: Record<string, ValueType | null>;
};

export type DecoratedTruncate = Omit<Truncate, 'relationIds'> & {
  relations: {
    namespace: string;
    name: string;
  }[];
};

export type PgOutputDecoratedMessageTypes =
  | Exclude<
      PgOutputMessageTypes,
      Relation | Insert | Update | Delete | Truncate
    >
  | DecoratedRelation
  | DecoratedInsert
  | DecoratedUpdate
  | DecoratedDelete
  | DecoratedTruncate;

type NamedParser = { name: string; parser: P.Parser<string, ValueType> };

type Chunk = { xlog: PgOutputMessageTypes } & unknown;

export const makeTypeDecorator: () => Transform = () => {
  const tableInfo = new Map<
    number,
    {
      namespace: string;
      name: string;
      colParsers: NamedParser[];
    }
  >();

  const convertTupleData = (tupleData: TupleData, parsers: NamedParser[]) =>
    tupleData.reduce((acc, item, index) => {
      const { name, parser } = parsers[index];

      const value = pipe(
        item,
        O.fold(
          () => undefined,
          (val) =>
            typeof val === 'string'
              ? pipe(
                  S.run(val)(parser),
                  E.fold(
                    () => val,
                    ({ value }) => value
                  )
                )
              : val
        )
      );

      if (value !== undefined) {
        return { ...acc, [name]: value };
      }
      return acc;
    }, {} as Record<string, ValueType | null>);

  return new Transform({
    objectMode: true,
    transform(chunk: Chunk, encoding, callback) {
      const { xlog } = chunk;
      switch (xlog.type) {
        case 'Relation': {
          const columns = xlog.columns.map((col) => {
            const dataTypeName = getTypeName(col.dataTypeId);
            return {
              ...col,
              dataTypeName
            };
          });
          const colParsers = xlog.columns.map((col) => ({
            name: col.name,
            parser: makeValueTypeParser(col.dataTypeId)
          }));
          const { namespace, name } = xlog;
          tableInfo.set(xlog.id, {
            namespace,
            name,
            colParsers
          });
          const relation: DecoratedRelation = { ...xlog, columns };
          this.push({ ...chunk, xlog: relation });
          callback();
          break;
        }
        case 'Insert': {
          const { type, relationId } = xlog;
          const relation = tableInfo.get(relationId);
          if (!relation) {
            callback(
              new PgOutputStateError(`Relation ${relationId} is unknown.`)
            );
            break;
          }
          const { namespace, name, colParsers } = relation;
          const newRecord = convertTupleData(xlog.newRecord, colParsers);
          const insert: DecoratedInsert = {
            type,
            namespace,
            name,
            newRecord
          };
          this.push({ ...chunk, xlog: insert });
          callback();
          break;
        }
        case 'Update': {
          const { type, relationId } = xlog;
          const relation = tableInfo.get(relationId);
          if (!relation) {
            callback(
              new PgOutputStateError(`Relation ${relationId} is unknown.`)
            );
            break;
          }
          const { namespace, name, colParsers } = relation;
          const newRecord = convertTupleData(xlog.newRecord, colParsers);
          const oldKey = pipe(
            xlog.oldKey,
            O.fold(
              () => undefined,
              (tuple) => convertTupleData(tuple, colParsers)
            )
          );
          const oldRecord = pipe(
            xlog.oldRecord,
            O.fold(
              () => undefined,
              (tuple) => convertTupleData(tuple, colParsers)
            )
          );
          const update: DecoratedUpdate = {
            type,
            namespace,
            name,
            oldKey,
            oldRecord,
            newRecord
          };
          this.push({ ...chunk, xlog: update });
          callback();
          break;
        }
        case 'Delete': {
          const { type, relationId } = xlog;
          const relation = tableInfo.get(relationId);
          if (!relation) {
            callback(
              new PgOutputStateError(`Relation ${relationId} is unknown.`)
            );
            break;
          }
          const { namespace, name, colParsers } = relation;
          const oldKey = pipe(
            xlog.oldKey,
            O.fold(
              () => undefined,
              (tuple) => convertTupleData(tuple, colParsers)
            )
          );
          const oldRecord = pipe(
            xlog.oldRecord,
            O.fold(
              () => undefined,
              (tuple) => convertTupleData(tuple, colParsers)
            )
          );
          const destroy: DecoratedDelete = {
            type,
            namespace,
            name,
            oldKey,
            oldRecord
          };
          this.push({ ...chunk, xlog: destroy });
          callback();
          break;
        }
        case 'Truncate': {
          const { type, options } = xlog;
          const notFound: number[] = [];
          const relations = xlog.relationIds.flatMap((relationId) => {
            const info = tableInfo.get(relationId);
            if (info) {
              const { namespace, name } = info;
              return [
                {
                  namespace,
                  name
                }
              ];
            }
            notFound.push(relationId);
            return [];
          });
          if (notFound.length > 0) {
            callback(
              new PgOutputStateError(
                `${notFound.length} relation(s) unknown: ${notFound}`
              )
            );
            break;
          }
          const truncate: DecoratedTruncate = {
            type,
            relations,
            options
          };
          this.push({ ...chunk, xlog: truncate });
          callback();
          break;
        }
        default: {
          this.push(chunk); // pass thru
          callback();
          break;
        }
      }
    }
  });
};
