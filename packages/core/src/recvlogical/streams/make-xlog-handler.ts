/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as E from 'fp-ts/Either';
import { Duplex } from 'stream';
import * as _ from 'lodash';
import { isNonEmpty } from 'fp-ts/lib/Array';
import { pipe } from 'fp-ts/lib/function';
import { PgXlogHandlerError } from '../../errors';
import { PgOutputDecoratedMessageTypes } from './make-type-decorator';
import { Logger, RecvlogicalOptions } from '../../types';

type Chunk = { walPos: bigint; xlog: PgOutputDecoratedMessageTypes };

const DEFAULT_CONCURRENCY = 1;

const CHUNK_BUFFER_SIZE = 100;

export const makeXlogHandler: (
  options: RecvlogicalOptions
) => (logger: Logger) => Duplex =
  ({ xlogs: { handler, concurrency = DEFAULT_CONCURRENCY } }) =>
  (logger) => {
    const pending: [bigint, boolean][] = [];
    let callback: (error?: Error | null) => void = _.once(_.noop);
    let full = false;

    class XlogHandler extends Duplex {
      constructor() {
        super({
          objectMode: true,
          writableHighWaterMark: CHUNK_BUFFER_SIZE,
          writev(chunks: { chunk: Chunk }[], cb) {
            callback = _.once(cb);

            if (!isNonEmpty(chunks)) {
              callback();
              return;
            }

            const logs = chunks.map(({ chunk }) => chunk);

            const lsns = logs.map(({ walPos }) => walPos);

            pending.push(...lsns.map((lsn): [bigint, boolean] => [lsn, true]));

            handler(logs)({ logger })().then((result) => {
              pipe(
                result,
                E.fold(
                  (error) => {
                    this.destroy(new PgXlogHandlerError(error));
                  },
                  (confirmed) => {
                    (confirmed ?? lsns).forEach((walPos) => {
                      const item = pending.find((p) => p[0] === walPos && p[1]);
                      if (item) {
                        item[1] = false;
                      }
                    });

                    let lastDone: bigint | undefined;
                    while (pending.length > 0 && !pending.at(0)?.[1]) {
                      const done = pending.shift()?.[0];
                      if (done !== undefined && done !== 0n) {
                        lastDone = done;
                      }
                    }

                    if (lastDone) {
                      if (!this.push(lastDone)) {
                        full = true;
                        return;
                      }
                    }

                    callback();
                  }
                )
              );
            });

            const pendingSize = pending.filter(([, pending]) => pending).length;

            if (pendingSize < concurrency && !full) {
              callback();
            }
          },
          read() {
            if (full) {
              full = false;
              callback();
            }
          }
        });
      }
    }

    const xlogHandler = new XlogHandler();

    xlogHandler.once('finish', () => {
      xlogHandler.push(null);
    });

    return xlogHandler;
  };
