import { Transform } from 'stream';
import {
  CopyBothResponse,
  PgServerMessageTypes,
  XKeepAlive
} from '../../pg-protocol';
import { PgBackendError, PgUnexpectedMessagesError } from '../../errors';
import { Logger } from '../../types';
import { logBackendMessage } from '../../utils';

export const makeCopyDataFilter: (
  logger: Logger
) => CopyDataFilter & Transform = (logger) => {
  class CopyDataFilter extends Transform {
    constructor() {
      super({
        objectMode: true,
        transform(chunk: PgServerMessageTypes, encoding, callback) {
          switch (chunk.type) {
            case 'ErrorResponse': {
              logBackendMessage(logger, chunk);
              callback(new PgBackendError([chunk]));
              break;
            }
            case 'CopyBothResponse': {
              this.emit('copyBothResponse', chunk);
              callback();
              break;
            }
            case 'CopyData': {
              if (chunk.payload.type === 'XKeepAlive') {
                this.emit('keepAlive', chunk.payload);
                callback();
              }
              if (chunk.payload.type === 'XLogData') {
                const { walStart, payload } = chunk.payload;
                this.push({ walPos: walStart, xlog: payload });
                callback();
              }
              break;
            }
            case 'NoticeResponse': {
              logBackendMessage(logger, chunk);
              callback();
              break;
            }
            default: {
              callback(new PgUnexpectedMessagesError([chunk]));
            }
          }
        }
      });
    }
  }

  return new CopyDataFilter();
};

interface CopyDataFilterEvents {
  keepAlive: (msg: XKeepAlive) => void;
  copyBothResponse: (msg: CopyBothResponse) => void;
}

declare interface CopyDataFilter {
  on<U extends keyof CopyDataFilterEvents>(
    event: U,
    listener: CopyDataFilterEvents[U]
  ): this;

  once<U extends keyof CopyDataFilterEvents>(
    event: U,
    listener: CopyDataFilterEvents[U]
  ): this;

  emit<U extends keyof CopyDataFilterEvents>(
    event: U,
    ...args: Parameters<CopyDataFilterEvents[U]>
  ): boolean;
}
