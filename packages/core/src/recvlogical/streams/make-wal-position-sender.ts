import { Transform } from 'stream';
import { Query } from '../../pg-protocol';
import { query, queryAndDecode } from '../../query';
import { PgContext, RecvlogicalOptions } from '../../types';
import { walLsnFromString } from './wal-lsn-from-string';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as O from 'fp-ts/Option';
import { pipe } from 'fp-ts/lib/function';
import { nonEmptyArray } from 'io-ts-types/nonEmptyArray';
import * as t from 'io-ts';
import { PgRecvlogicalSetupError } from '../../errors';

export const makeWalPositionSender: (
  options: RecvlogicalOptions
) => RTE.ReaderTaskEither<PgContext, Error, Transform> = ({
  slotName,
  publicationName,
  createTempSlotIfNone,
  createSlotIfNone,
  createPublicationIfNone,
  includeTables
}) => {
  const createSlot = pipe(
    queryAndDecode(
      `CREATE_REPLICATION_SLOT ${slotName} ${
        createTempSlotIfNone ? 'TEMPORARY ' : ''
      }LOGICAL pgoutput NOEXPORT_SNAPSHOT`,
      nonEmptyArray(
        t.type({
          consistent_point: walLsnFromString
        })
      )
    ),
    RTE.map((rows) => rows[0].consistent_point)
  );

  const createPublication = query(
    `CREATE PUBLICATION ${publicationName} ${
      includeTables ? `FOR TABLE ${includeTables.join(', ')}` : 'FOR ALL TABLES'
    }`
  );

  const getPublication = pipe(
    query(`SELECT * FROM pg_publication WHERE pubname = '${publicationName}'`),
    RTE.chain((rows) =>
      rows.length > 0
        ? RTE.of(O.some(rows[0]))
        : createPublicationIfNone
        ? RTE.of(O.none)
        : ({ logger }) => {
            const err = new Error('No existing publication.');
            logger.error({ err, publicationName });
            return TE.left(new PgRecvlogicalSetupError(err));
          }
    )
  );

  const getConfirmedFlushLsn = pipe(
    queryAndDecode(
      `SELECT confirmed_flush_lsn FROM pg_replication_slots WHERE slot_name = '${slotName}'`,
      t.array(
        t.type({
          confirmed_flush_lsn: walLsnFromString
        })
      )
    ),
    RTE.chain((rows) =>
      rows.length > 0
        ? RTE.of(O.some(rows[0].confirmed_flush_lsn))
        : createTempSlotIfNone || createSlotIfNone
        ? RTE.of(O.none)
        : ({ logger }) => {
            const err = new Error('No existing slot.');
            logger.error({ err, slotName });
            return TE.left(new PgRecvlogicalSetupError(err));
          }
    )
  );

  return pipe(
    getConfirmedFlushLsn,
    RTE.bindTo('confirmedFlushLsn'),
    RTE.bind('publication', () => getPublication),
    RTE.chainFirst(({ publication }) =>
      pipe(
        publication,
        O.fold(
          () => createPublication,
          (pub) => RTE.of([pub])
        )
      )
    ),
    RTE.bind('lsn', ({ confirmedFlushLsn }) =>
      pipe(
        confirmedFlushLsn,
        O.fold(
          () => createSlot,
          (lsn) => RTE.of(lsn)
        )
      )
    ),
    RTE.map(({ lsn }) => {
      let lastWalPos = lsn;

      class WalPositionSender extends Transform {
        constructor() {
          super({
            objectMode: true,
            construct(callback) {
              const sql = `START_REPLICATION SLOT ${slotName} LOGICAL ${walLsnFromString.encode(
                lastWalPos
              )} (proto_version '1', publication_names '${publicationName}')`;
              const query: Query = {
                type: 'Query',
                sql
              };
              this.push(query);
              callback();
            },
            transform(walPos: bigint, encoding, callback) {
              if (walPos > lastWalPos) {
                lastWalPos = walPos;
              }

              const timeStamp =
                BigInt(new Date().getTime() - Date.UTC(2000, 0, 1)) * 1000n;

              this.push({
                type: 'CopyData',
                payload: {
                  type: 'XStatusUpdate',
                  lastWalWrite: lastWalPos,
                  lastWalFlush: lastWalPos,
                  lastWalApply: 0n,
                  replyNow: false,
                  timeStamp
                }
              });

              callback();
            }
          });
        }
      }

      const walPositionSender = new WalPositionSender();

      return walPositionSender;
    })
  );
};
