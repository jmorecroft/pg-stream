import { Readable } from 'stream';
import * as _ from 'lodash';
import { RecvlogicalOptions } from '../../types';

const DEFAULT_INTERVAL = 10000; // Every 10 seconds

export type NextFn = _.DebouncedFunc<() => boolean>;

export const makeHeartbeat: (options: RecvlogicalOptions) => {
  heartbeat: Readable;
  next: NextFn;
} = ({ heartbeatInterval = DEFAULT_INTERVAL }) => {
  class Heartbeat extends Readable {
    constructor() {
      super({
        objectMode: true,
        read() {
          next();
        },
        destroy() {
          next.cancel();
        }
      });
    }
  }

  const heartbeat = new Heartbeat();

  const next = _.throttle(() => heartbeat.push(0n), heartbeatInterval, {
    trailing: true
  });

  return { heartbeat, next };
};
