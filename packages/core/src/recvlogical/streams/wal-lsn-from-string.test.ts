import { walLsnFromString } from './wal-lsn-from-string';
import * as E from 'fp-ts/Either';

describe(__filename, () => {
  it('should encode/decode', () => {
    expect(walLsnFromString.decode('ABF/DEF')).toEqual(E.right(0xabf00000defn));
    expect(walLsnFromString.decode('0/DEF')).toEqual(E.right(0xdefn));
    expect(walLsnFromString.decode('0/000')).toEqual(E.right(0n));
    expect(walLsnFromString.encode(0xabf00000defn)).toEqual('ABF/DEF');
    expect(walLsnFromString.encode(0xdefn)).toEqual('0/DEF');
  });
});
