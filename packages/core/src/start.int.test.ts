import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import * as B from './parser/buffer';
import * as P from 'parser-ts/Parser';
import { pipe } from 'fp-ts/lib/function';
import {
  BackendKeyData,
  NoticeResponse,
  ParameterStatus,
  pgClientMessageParser,
  PgClientMessageTypes,
  ReadyForQuery,
  StartupMessage,
  startupMessage,
  makePgReadyForQuery,
  makePgAuthenticateOk,
  makePgAuthenticateCleartextPassword,
  makePgBackendKeyData,
  makePgNoticeResponse,
  makePgParameterStatus
} from './pg-protocol';
import { start } from './start';
import { open } from './open';
import { close } from './close';
import { BaseOptions } from './types';
import { makeTestServer, TestServer, logger } from 'test-utils';

describe(__filename, () => {
  let server: TestServer<BaseOptions>;

  const options: BaseOptions = {
    host: 'localhost',
    port: 12345,
    password: 'password',
    username: 'username',
    database: 'database'
  };

  beforeEach(() => {
    server = makeTestServer(options);
  });

  afterEach((done) => {
    server.close(done);
  });

  const noticeResponse: NoticeResponse = {
    type: 'NoticeResponse',
    notices: [
      {
        type: 'X',
        value: 'hello'
      }
    ]
  };

  const backendKeyData: BackendKeyData = {
    type: 'BackendKeyData',
    pid: 123,
    secretKey: 234
  };

  const parameterStatus: ParameterStatus = {
    name: 'param1',
    value: 'value1',
    type: 'ParameterStatus'
  };

  const readyForQuery: ReadyForQuery = {
    type: 'ReadyForQuery',
    transactionStatus: 'E'
  };

  it('should startup', (done) => {
    server.negotiateSSL((socket) => {
      let parser: P.Parser<number, StartupMessage | PgClientMessageTypes> =
        startupMessage;
      socket.on('data', (buf) => {
        pipe(
          parser(B.stream(buf)),
          E.fold(
            (err) => {
              done(err);
            },
            ({ value }) => {
              if (value.type === 'StartupMessage') {
                expect(value).toEqual({
                  type: 'StartupMessage',
                  protocolVersion: 196608,
                  parameters: [
                    {
                      name: 'database',
                      value: 'database'
                    },
                    {
                      name: 'user',
                      value: 'username'
                    }
                  ]
                });

                parser = pgClientMessageParser;
                socket.write(makePgAuthenticateCleartextPassword());
              }

              if (value.type === 'PasswordMessage') {
                expect(value).toEqual({
                  type: 'PasswordMessage',
                  password: 'password'
                });

                socket.write(makePgAuthenticateOk());
                socket.write(makePgBackendKeyData(backendKeyData));
                socket.write(makePgNoticeResponse(noticeResponse));
                socket.write(makePgParameterStatus(parameterStatus));
                socket.write(makePgReadyForQuery(readyForQuery));
              }
            }
          )
        );
      });
    });

    server.listen((options) => {
      TE.bracket(
        open({ options, logger }),
        (conn) => start(options, false)({ conn, logger }),
        (conn) => close({ conn, logger })
      )().then((result) => {
        pipe(
          result,
          E.fold(
            (err) => done(err),
            (messages) => {
              expect(messages).toEqual([
                backendKeyData,
                parameterStatus,
                readyForQuery
              ]);
              done();
            }
          )
        );
      });
    });
  });
});
