import { Socket } from 'net';
import { Readable, Writable } from 'stream';
import * as RTE from 'fp-ts/ReaderTaskEither';
import { createLogger } from 'bunyan';
import { PgOutputDecoratedMessageTypes } from './recvlogical';

export interface BaseOptions {
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
  useSSL?: boolean;
}

export type XlogHandler = (
  xlogs: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[]
) => RTE.ReaderTaskEither<
  {
    logger: Logger;
  },
  Error,
  bigint[] | undefined
>;

export type RecvlogicalOptions =
  | BaseOptions & {
      slotName: string;
      publicationName: string;
      heartbeatInterval?: number;
      xlogs: {
        handler: XlogHandler;
        concurrency?: number;
      };
      createTempSlotIfNone?: boolean;
      createSlotIfNone?: boolean;
      createPublicationIfNone?: boolean;
      includeTables?: string[];
      endAfter?: { duration: number };
    };

export interface PgConnection {
  // raw socket to Postgres
  pgSocket: Socket;
  // read message objects from Postgres
  pgRead: Readable;
  // write message objects to Postgres
  pgWrite: Writable;
  // submit processors (duplex streams) to this sink
  processorWrite: Writable;
}

export type PgContext = {
  conn: PgConnection;
  logger: Logger;
};

export type Logger = ReturnType<typeof createLogger>;
