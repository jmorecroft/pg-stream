import Logger from 'bunyan';
import { ErrorResponse, NoticeResponse } from './pg-protocol';

export const logBackendMessage = (
  logger: Logger,
  message: NoticeResponse | ErrorResponse
) => {
  const pairs =
    message.type === 'NoticeResponse' ? message.notices : message.errors;

  const { log, fields, msg } = pairs.reduce(
    ({ log, fields, msg }, { type, value }) => {
      switch (type) {
        case 'V': {
          const log2 =
            {
              ERROR: logger.error.bind(logger),
              FATAL: logger.fatal.bind(logger),
              PANIC: logger.fatal.bind(logger),
              WARNING: logger.warn.bind(logger),
              NOTICE: logger.info.bind(logger),
              DEBUG: logger.debug.bind(logger),
              INFO: logger.info.bind(logger),
              LOG: logger.info.bind(logger)
            }[value] ?? log;
          return { log: log2, fields, msg };
        }
        case 'C': {
          return { log, fields: { ...fields, code: value }, msg };
        }
        case 'M': {
          return { log, fields, msg: `POSTGRES: ${value}` };
        }
        case 'D': {
          return { log, fields: { ...fields, detail: value }, msg };
        }
        case 'H': {
          return { log, fields: { ...fields, advice: value }, msg };
        }
        case 'P': {
          return { log, fields: { ...fields, queryPosition: value }, msg };
        }
        case 'p': {
          return {
            log,
            fields: { ...fields, internalQueryPosition: value },
            msg
          };
        }
        case 'q': {
          return { log, fields: { ...fields, internalQuery: value }, msg };
        }
        case 'W': {
          return { log, fields: { ...fields, context: value }, msg };
        }
        case 's': {
          return { log, fields: { ...fields, schema: value }, msg };
        }
        case 't': {
          return { log, fields: { ...fields, table: value }, msg };
        }
        case 'c': {
          return { log, fields: { ...fields, column: value }, msg };
        }
        case 'd': {
          return { log, fields: { ...fields, dataType: value }, msg };
        }
        case 'n': {
          return { log, fields: { ...fields, constraint: value }, msg };
        }
        case 'F': {
          return { log, fields: { ...fields, file: value }, msg };
        }
        case 'L': {
          return { log, fields: { ...fields, line: value }, msg };
        }
        case 'R': {
          return { log, fields: { ...fields, routine: value }, msg };
        }
      }
      return { log, fields, msg };
    },
    {
      log: logger.info.bind(logger),
      fields: {},
      msg: 'Message received from backend.'
    }
  );

  log(fields, msg);
};
