import { pipe } from 'fp-ts/lib/function';
import { open } from './open';
import { close } from './close';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import {
  query as queryParser,
  DataRow,
  pgClientMessageParser,
  ReadyForQuery,
  RowDescription,
  makePgDataRow,
  makePgReadyForQuery,
  makePgRowDescription,
  PgTypes
} from './pg-protocol';
import * as t from 'io-ts';
import { query, queryAndDecode } from './query';
import * as B from './parser/buffer';
import { BaseOptions } from './types';
import { TestServer, makeTestServer, logger } from 'test-utils';

describe(__filename, () => {
  let server: TestServer<BaseOptions>;

  const options: BaseOptions = {
    host: 'localhost',
    port: 12345,
    password: 'password',
    username: 'username',
    database: 'database'
  };

  beforeEach(() => {
    server = makeTestServer(options);
  });

  afterEach((done) => {
    server.close(done);
  });

  const rowDescription: RowDescription = {
    type: 'RowDescription',
    fields: [
      {
        name: 'myint',
        tableId: 1,
        columnId: 2,
        dataTypeId: PgTypes.int4.baseTypeOid,
        dataTypeModifier: 4,
        dataTypeSize: 5,
        format: 6
      },
      {
        name: 'mystring',
        tableId: 7,
        columnId: 8,
        dataTypeId: PgTypes.varchar.baseTypeOid,
        dataTypeModifier: 10,
        dataTypeSize: 11,
        format: 12
      },
      {
        name: 'myfloat',
        tableId: 13,
        columnId: 14,
        dataTypeId: PgTypes.float8.baseTypeOid,
        dataTypeModifier: 15,
        dataTypeSize: 16,
        format: 17
      },
      {
        name: 'mybools',
        tableId: 18,
        columnId: 19,
        dataTypeId: PgTypes.bool.arrayTypeOid,
        dataTypeModifier: 20,
        dataTypeSize: 21,
        format: 22
      }
    ]
  };

  const dataRow: DataRow = {
    type: 'DataRow',
    values: ['123', 'hello', '1.23', '{t,f,t}']
  };

  const decoder = t.array(
    t.type({
      myint: t.union([t.Int, t.null]),
      mystring: t.union([t.string, t.null]),
      myfloat: t.union([t.number, t.null]),
      mybools: t.union([t.array(t.boolean), t.null])
    })
  );

  const readyForQuery: ReadyForQuery = {
    type: 'ReadyForQuery',
    transactionStatus: 'I'
  };

  it('should query', (done) => {
    server.negotiateSSL((socket) => {
      socket.on('data', (buf) => {
        pipe(
          pgClientMessageParser(B.stream(buf)),
          E.fold(
            (err) => {
              done(err);
            },
            ({ value }) => {
              expect(value).toEqual({
                type: 'Query',
                sql: 'select hello'
              });

              const buf = makePgRowDescription(rowDescription);
              const buf1 = buf.slice(0, buf.length / 2);
              socket.write(buf1);
              setTimeout(() => {
                const buf2 = buf.slice(buf.length / 2);
                const buf3 = makePgDataRow(dataRow);
                const buf4 = makePgReadyForQuery(readyForQuery);
                socket.write(buf2);
                setTimeout(() => {
                  socket.write(Buffer.concat([buf3, buf4]));
                }, 100);
              }, 100);
            }
          )
        );
      });
    });

    server.listen((options) => {
      TE.bracket(
        open({ options, logger }),
        (conn) => queryAndDecode('select hello', decoder)({ conn, logger }),
        (conn) => close({ conn, logger })
      )().then((result) => {
        pipe(
          result,
          E.fold(
            (err) => done(err),
            (messages) => {
              expect(messages).toEqual([
                {
                  myint: 123,
                  mystring: 'hello',
                  myfloat: 1.23,

                  mybools: [true, false, true]
                }
              ]);
              done();
            }
          )
        );
      });
    });
  });

  it('should handle multiple simultaneous queries', (done) => {
    const dataRows: DataRow[] = [
      { type: 'DataRow', values: ['1', 'hi', '2.3', '{t}'] },
      { type: 'DataRow', values: ['2', 'ho', '3.4', '{t,t}'] },
      { type: 'DataRow', values: ['3', 'sup', '4.5', null] }
    ];

    server.negotiateSSL((socket) => {
      socket.on('data', (buf) => {
        pipe(
          queryParser(B.stream(buf)),
          E.fold(
            (err) => {
              done(err);
            },
            ({ value }) => {
              const replies: Record<string, DataRow> = {
                'hello there': dataRows[0],
                hi: dataRows[1],
                yo: dataRows[2]
              };
              const reply = replies[value.sql];
              expect(reply).toBeDefined();
              socket.write(makePgRowDescription(rowDescription));
              socket.write(makePgDataRow(reply));
              socket.write(makePgReadyForQuery(readyForQuery));
            }
          )
        );
      });
    });

    server.listen((options) => {
      TE.bracket(
        open({ options, logger }),
        (conn) =>
          RTE.sequenceArray([
            queryAndDecode('hello there', decoder),
            query('hi'),
            query('yo')
          ])({ conn, logger }),
        (conn) => close({ conn, logger })
      )().then((result) => {
        pipe(
          result,
          E.fold(
            (err) => done(err),
            (messages) => {
              expect(messages).toEqual([
                [
                  {
                    myint: 1,
                    mystring: 'hi',
                    myfloat: 2.3,
                    mybools: [true]
                  }
                ],
                [
                  {
                    myint: 2,
                    mystring: 'ho',
                    myfloat: 3.4,
                    mybools: [true, true]
                  }
                ],
                [{ myint: 3, mystring: 'sup', myfloat: 4.5, mybools: null }]
              ]);
              done();
            }
          )
        );
      });
    });
  });

  it('should fail on parse error', (done) => {
    server.negotiateSSL((socket) => {
      socket.on('data', (buf) => {
        pipe(
          pgClientMessageParser(B.stream(buf)),
          E.fold(
            (err) => {
              done(err);
            },
            ({ value }) => {
              expect(value).toEqual({
                type: 'Query',
                sql: 'select hello'
              });

              const buf = makePgRowDescription(rowDescription);
              buf.writeInt32BE(10, 1); // change length to 2 bytes
              socket.write(buf);
            }
          )
        );
      });
    });

    server.listen((options) => {
      TE.bracket(
        open({ options, logger }),
        (conn) => query('select hello')({ conn, logger }),
        (conn) => close({ conn, logger })
      )().then((result) => {
        pipe(
          result,
          E.fold(
            (err) => {
              expect(err).toEqual(
                expect.objectContaining({
                  name: 'PgParseError'
                })
              );
              done();
            },
            () => {
              fail();
            }
          )
        );
      });
    });
  });
});
