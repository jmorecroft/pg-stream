import {
  ErrorResponse,
  PgServerMessageTypes
} from './pg-protocol/message-parsers';
import * as t from 'io-ts';
import { failure } from 'io-ts/PathReporter';
import * as RNEA from 'fp-ts/lib/ReadonlyNonEmptyArray';
import { CustomError } from 'ts-custom-error';
import { ParseError } from 'parser-ts/lib/ParseResult';

export class PgConnectError extends CustomError {
  constructor(readonly cause: Error) {
    super(`Error connecting to Postgres: ${cause.message}`);
  }
}

export class PgParseError extends CustomError {
  constructor(readonly error: ParseError<unknown>) {
    super(`Error parsing data from Postgres: ${error}`);
  }
}

export class PgStreamError extends CustomError {
  constructor(readonly cause: Error) {
    super(`Error streaming from/to Postgres: ${cause.message}`);
  }
}

export class PgQueryAbortedError extends CustomError {
  constructor(readonly received: PgServerMessageTypes[]) {
    super(`Query aborted after ${received.length} message(s) received.`);
  }
}

export class PgBackendError extends CustomError {
  constructor(readonly errors: RNEA.ReadonlyNonEmptyArray<ErrorResponse>) {
    super(`Postgres error(s) (${errors.length}) reported.`);
  }
}

export class PgSaslError extends CustomError {
  constructor(readonly reason: string) {
    super(`Error during SASL authentication negotiation: ${reason}`);
  }
}

export class PgOutputStateError extends CustomError {
  constructor(readonly reason: string) {
    super(`Error processing stateful pgoutput messages: ${reason}`);
  }
}

export class PgRowValidationError extends CustomError {
  errors: string[];

  constructor(errors: t.Errors) {
    const errorStrings = failure(errors);
    super(
      errorStrings.length === 1
        ? `Row validation error: ${errorStrings[0]}`
        : `Row validation errors (${errorStrings.length}) reported.`
    );
    this.errors = errorStrings;
  }
}

export class PgUnexpectedMessagesError extends CustomError {
  constructor(
    readonly messages: RNEA.ReadonlyNonEmptyArray<PgServerMessageTypes>
  ) {
    super(`Postgres returned ${messages.length} unexpected message(s).`);
  }
}

export class PgQueryNoResultsError extends CustomError {
  constructor() {
    super('Query did not return any results.');
  }
}

export class PgXlogHandlerError extends CustomError {
  constructor(readonly cause: Error) {
    super(`Xlog handler error: ${cause.message}`);
  }
}

export class PgRecvlogicalSetupError extends CustomError {
  constructor(readonly cause: Error) {
    super(`Error during recvlogical setup: ${cause.message}`);
  }
}
