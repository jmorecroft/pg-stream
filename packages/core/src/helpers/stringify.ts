/**
 * Simple wrapper of JSON.stringify that handles stringification of bigints, which is
 * not supported by JSON.stringify.
 *
 * @param object
 * @returns
 */
export const stringify = (object: unknown) => {
  return JSON.stringify(object, (key, value) => {
    if (typeof value === 'bigint') {
      return String(value);
    }
    return value;
  });
};
