## pg-stream-core

A limited-functionality PostgreSQL client library for simple queries and most significantly, reliable logical replication streaming using the pgoutput protocol.

The [pg-stream](https://www.npmjs.com/package/@jmorecroft67/pg-stream-cli) CLI utility is the simplest way to use the functionality in pg-stream without writing any code. If you'd like to use it directly or just understand it a little better, read on.

This lib uses NodeJS streams heavily to perform conversations with the PostgreSQL server, hence the name. For logical replication this means we're able to nicely implement backpressure, which means our lib shouldn't explode with memory if the rate of processing of transaction logs is slower than the incoming rate of transaction logs. We also run the logical replication stream in a Duplex loop, so successful processing of logs by the selected handler results in the respective WAL positions being pushed as confirmed back to the server.

This client is pure JavaScript and uses a small set of dependencies, mostly the awesome functional programming (FP) libraries by [Gulio Canti](https://github.com/gcanti) and friends ([io-ts](https://www.npmjs.com/package/io-ts), [fp-ts](https://www.npmjs.com/package/fp-ts), [parser-ts](https://www.npmjs.com/package/parser-ts)).

### structure

The pg-stream lib is simply a collection of functions:

- _open_ - opens a connection to server, including any required SSL negotiation
- _start_ - performs the client-server startup sequence, which includes any authentication exchanges (only trust, legacy MD5 and SASL SCRAM-SHA-256 are supported) and the informing by the server of parameter status values
- _query_ - executes an SQL statement on the server
- _queryAndDecode_ - same as `query` but performs an additional decode step on the results
- _recvlogical_ - starts a logical replication stream from the server
- _close_ - closes the connection to the server

These functions are then stitched together in FP pipes.

### query

```typescript
import * as t from 'io-ts';
import * as pg from '@jmorecroft67/pg-stream-core';
import { pipe } from 'fp-ts/function';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import { createLogger } from 'bunyan';

export const logger = createLogger({
  name: 'test',
  level: 'info'
});

const options: pg.BaseOptions = {
  host: 'localhost',
  port: 5432,
  database: 'postgres',
  username: 'postgres',
  password: 'topsecret'
};

TE.bracket(
  pg.open({ options, logger }),
  (conn) =>
    pipe(
      { conn, logger },
      pipe(
        pg.start(options, false),
        RTE.bindTo('startup'),
        RTE.bind('result', () =>
          RTE.sequenceSeqArray([
            pg.query(
              'create table test (id integer primary key, greeting varchar)'
            ),
            pg.query('alter table test replica identity full'),
            pg.query(`insert into test values (1, 'hello'), (2, 'gday')`),
            pg.query(`update test set greeting = 'hiya' where id = 1`),
            pg.queryAndDecode(
              'select * from test',
              t.array(t.type({ id: t.number, greeting: t.string }))
            ),
            pg.query('delete from test where id = 1'),
            pg.query('drop table test')
          ])
        )
      )
    ),
  (conn) => pg.close({ conn, logger })
)().then((result) => {
  pipe(
    result,
    E.fold(
      (error) => console.error('failed:', error),
      ({ startup, result }) => {
        console.log('startup:', startup);
        console.log('result:', result);
      }
    )
  );
});
```

This program is best demonstrated by piping output through the [bunyan](https://www.npmjs.com/package/bunyan) utility, which detects and nicely formats bunyan log lines while leaving the rest untouched:

```bash
$ $(npm bin)/ts-node src/test-query.ts | bunyan
```

The output is:

```
[2022-05-23T01:12:28.943Z]  WARN: test/8276 on Jesss-MBP: SSL is not enabled on the Postgres server but connecting anyway.
[2022-05-23T01:12:28.944Z]  INFO: test/8276 on Jesss-MBP: Connection established. (host=localhost, port=5432)
[2022-05-23T01:12:29.009Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="CREATE TABLE")
[2022-05-23T01:12:29.009Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.012Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="ALTER TABLE")
[2022-05-23T01:12:29.012Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.017Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="INSERT 0 2")
[2022-05-23T01:12:29.017Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.019Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="UPDATE 1")
[2022-05-23T01:12:29.019Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.022Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="SELECT 2")
[2022-05-23T01:12:29.022Z]  INFO: test/8276 on Jesss-MBP: Query complete.
    result: [{"id":2,"greeting":"gday"},{"id":1,"greeting":"hiya"}]
[2022-05-23T01:12:29.024Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="DELETE 1")
[2022-05-23T01:12:29.024Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.030Z]  INFO: test/8276 on Jesss-MBP: Command complete. (tag="DROP TABLE")
[2022-05-23T01:12:29.030Z]  INFO: test/8276 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:12:29.030Z]  INFO: test/8276 on Jesss-MBP: Connection closed.
startup: [
  { type: 'ParameterStatus', name: 'application_name', value: '' },
  { type: 'ParameterStatus', name: 'client_encoding', value: 'UTF8' },
  { type: 'ParameterStatus', name: 'DateStyle', value: 'ISO, MDY' },
  {
    type: 'ParameterStatus',
    name: 'default_transaction_read_only',
    value: 'off'
  },
  { type: 'ParameterStatus', name: 'in_hot_standby', value: 'off' },
  { type: 'ParameterStatus', name: 'integer_datetimes', value: 'on' },
  { type: 'ParameterStatus', name: 'IntervalStyle', value: 'postgres' },
  { type: 'ParameterStatus', name: 'is_superuser', value: 'on' },
  { type: 'ParameterStatus', name: 'server_encoding', value: 'UTF8' },
  { type: 'ParameterStatus', name: 'server_version', value: '14.2' },
  {
    type: 'ParameterStatus',
    name: 'session_authorization',
    value: 'postgres'
  },
  {
    type: 'ParameterStatus',
    name: 'standard_conforming_strings',
    value: 'on'
  },
  { type: 'ParameterStatus', name: 'TimeZone', value: 'GMT' },
  { type: 'BackendKeyData', pid: 39, secretKey: -1122862277 },
  { type: 'ReadyForQuery', transactionStatus: 'I' }
]
result: [
  [],
  [],
  [],
  [],
  [ { id: 2, greeting: 'gday' }, { id: 1, greeting: 'hiya' } ],
  [],
  []
]
```

### recvlogical

```typescript
import * as pg from '@jmorecroft67/pg-stream-core';
import { pipe } from 'fp-ts/function';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import { createLogger } from 'bunyan';

export const logger = createLogger({
  name: 'test',
  level: 'info'
});

const options: pg.RecvlogicalOptions = {
  host: 'localhost',
  port: 5432,
  database: 'postgres',
  username: 'postgres',
  password: 'topsecret',
  publicationName: 'testpub',
  slotName: 'testslot',
  xlogs: {
    handler: pg.stdoutHandler
  },
  createTempSlotIfNone: true,
  createPublicationIfNone: true
};

TE.bracket(
  pg.open({ options, logger }),
  (conn) =>
    pipe(
      { conn, logger },
      pipe(
        pg.start(options, true),
        RTE.chain(() => pg.recvlogical(options))
      )
    ),
  (conn) => pg.close({ conn, logger })
)();
```

If we run this program then once initialised it will forever wait for transaction logs from the database, which it will send immediately to stdout. We therefore can demonstrate this program by running in the background, then running the first program. We'll then shut down the background job gracefully by sending SIGINT (ie. the CTRL-C signal) to the running node process.

```bash
$ ( $(npm bin)/ts-node src/test-recvlogical.ts | bunyan ) &
```

```
[2022-05-23T01:13:23.476Z]  WARN: test/8310 on Jesss-MBP: SSL is not enabled on the Postgres server but connecting anyway.
[2022-05-23T01:13:23.477Z]  INFO: test/8310 on Jesss-MBP: Connection established. (host=localhost, port=5432)
[2022-05-23T01:13:23.531Z]  INFO: test/8310 on Jesss-MBP: Command complete. (tag="SELECT 0")
[2022-05-23T01:13:23.531Z]  INFO: test/8310 on Jesss-MBP: Query complete. (result=[])
[2022-05-23T01:13:23.535Z]  INFO: test/8310 on Jesss-MBP: Command complete. (tag="SELECT 1")
[2022-05-23T01:13:23.535Z]  INFO: test/8310 on Jesss-MBP: Query complete.
    result: [{"oid":"16582","pubname":"testpub","pubowner":"10","puballtables":true,"pubinsert":true,"pubupdate":true,"pubdelete":true,"pubtruncate":true,"pubviaroot":false}]
[2022-05-23T01:13:23.546Z]  INFO: test/8310 on Jesss-MBP: Command complete. (tag=CREATE_REPLICATION_SLOT)
[2022-05-23T01:13:23.546Z]  INFO: test/8310 on Jesss-MBP: Query complete.
    result: [{"slot_name":"testslot","consistent_point":"0/1E12580","snapshot_name":null,"output_plugin":"pgoutput"}]
```

```bash
$ $(npm bin)/ts-node src/test-query.ts > /dev/null
```

```
{"type":"Begin","finalLsn":"31556328","timeStamp":"706583643744438","tranId":891}
{"type":"Commit","lsn":"31556328","endLsn":"31557664","timeStamp":"706583643744438"}
{"type":"Begin","finalLsn":"31558000","timeStamp":"706583643749056","tranId":892}
{"type":"Commit","lsn":"31558000","endLsn":"31558104","timeStamp":"706583643749056"}
{"type":"Begin","finalLsn":"31558464","timeStamp":"706583643752923","tranId":893}
{"type":"Relation","id":16597,"namespace":"public","name":"test","replIdent":"f","columns":[{"isKey":true,"name":"id","dataTypeId":23,"typeMod":-1,"dataTypeName":"int4"},{"isKey":true,"name":"greeting","dataTypeId":1043,"typeMod":-1,"dataTypeName":"varchar"}]}
{"type":"Insert","namespace":"public","name":"test","newRecord":{"id":1,"greeting":"hello"}}
{"type":"Insert","namespace":"public","name":"test","newRecord":{"id":2,"greeting":"gday"}}
{"type":"Commit","lsn":"31558464","endLsn":"31558512","timeStamp":"706583643752923"}
{"type":"Begin","finalLsn":"31558664","timeStamp":"706583643758238","tranId":894}
{"type":"Update","namespace":"public","name":"test","oldRecord":{"id":1,"greeting":"hello"},"newRecord":{"id":1,"greeting":"hiya"}}
{"type":"Commit","lsn":"31558664","endLsn":"31558712","timeStamp":"706583643758238"}
{"type":"Begin","finalLsn":"31558784","timeStamp":"706583643777656","tranId":895}
{"type":"Delete","namespace":"public","name":"test","oldRecord":{"id":1,"greeting":"hiya"}}
{"type":"Commit","lsn":"31558784","endLsn":"31558832","timeStamp":"706583643777656"}
{"type":"Begin","finalLsn":"31564816","timeStamp":"706583643783849","tranId":896}
{"type":"Commit","lsn":"31564816","endLsn":"31566016","timeStamp":"706583643783849"}
```

```bash
$ kill -SIGINT 8310
```

```
[2022-05-23T01:16:41.180Z]  INFO: test/8310 on Jesss-MBP: Logical streaming complete.
[2022-05-23T01:16:41.181Z]  INFO: test/8310 on Jesss-MBP: Connection closed.
```

#### transaction log (xlog) handlers

The recvlogical function ultimately delivers xlogs to the configured handler, which in the previous example is the provided stdout handler but may otherwise be a stderr handler, file handler or custom handler. In addition there is a concurrency setting, which defaults to 1 but may be overriden if the provided handler is capable of handling multiple batches in parallel. Note that the provided stdout, stderr and file handlers all only support the default concurrency setting of 1.

The handler function type is as follows, where the `PgOutputDecoratedMessageTypes` type is a discriminated union of types based on the [pgoutput protocol](https://www.postgresql.org/docs/current/protocol-logicalrep-message-formats.html) (v1):

```typescript
import * as RTE from 'fp-ts/ReaderTaskEither';

export type XlogHandler = (
  xlogs: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[]
) => RTE.ReaderTaskEither<
  {
    logger: Logger;
  },
  Error,
  bigint[] | undefined
>;
```

##### stdout and stderr handlers

The provided stdout and stderr handlers are implemented as simple stream handlers. They stringify each xlog, append an end-of-line character, then output to the stream. They complete a call once all xlogs have been added and the stream is ready to accept further data. This means that theoretically we could confirm a batch of xlogs to the Postgres server but the process might unexpectedly exit before the stream's internal buffer has been flushed, so we'd end up missing some xlogs. Given this is typically not a big deal for output to stdout/stderr this behaviour was considered a fair performance/simplicity trade-off.

##### file handler

Conversely this potential for missing xlogs, however small, is probably not acceptable for file output, which is why the file handler is implemented differently. The file handler stringifies and appends the end-of-line character also, but only completes a call once the last xlog has been confirmed as flushed from the internal file stream buffer. Further the file stream is initially opened using a mode of "as" (appending in synchronous mode), which should mean that once the stream buffer is flushed we can reliably infer that the data has been written to the file.

##### Google Cloud Platform (GCP) PubSub handler

The [pg-stream-gcp](https://www.npmjs.com/package/@jmorecroft67/pg-stream-gcp) library provides a handler for sending xlogs to a GCP PubSub topic.

### alternatives

This lib is very much a work in progress, has NOT been widely tested in the field and any use in a production environment should be carefully considered! This lib was written from a desire to have something simple, flexible and easily deployable to reliably push xlogs from Postgres, with backpressure support. It was also a great excuse to re-invent a few wheels and learn more about FP!

Thankfully there are a number of great, mature alternatives that may do what you're after.

- [pg](https://www.npmjs.com/package/pg) - the default choice of client for connecting to Postgres from JavaScript. For logical replication scenarios there is the [pg-copy-streams](https://www.npmjs.com/package/pg-copy-streams) lib built on top of this, which I initially investigated using before naively deciding to do it all myself!
- [psql](https://www.postgresql.org/docs/current/app-psql.html) - the standard Postgres interactive client. Logical replication using the SQL interface is demonstrated [here](https://www.postgresql.org/docs/current/logicaldecoding-example.html).
- [pg_recvlogical](https://www.postgresql.org/docs/current/app-pgrecvlogical.html) - a utility for logical replication streaming shipped with Postgres, and inspiration for the `recvlogical` function name! An example of its use with the wal2json plugin is [here](https://access.crunchydata.com/documentation/wal2json/2.0/). (Note to keep things simple the pg-stream-core lib only uses the built-in pgoutput plugin)
- [Debezium](https://debezium.io/) - the batteries-included solution for change data capture (CDC) from Postgres, or a bunch of other databases. This one is probably the one to choose if you're trying to implement CDC and require a battle-tested solution.
