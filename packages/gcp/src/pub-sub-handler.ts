import { XlogHandler, stringify } from '@jmorecroft67/pg-stream-core';
import * as E from 'fp-ts/Either';
import * as O from 'fp-ts/Option';
import * as RA from 'fp-ts/ReadonlyArray';
import * as T from 'fp-ts/Task';
import * as TE from 'fp-ts/TaskEither';
import * as R from 'retry-ts/Task';
import {
  constantDelay,
  capDelay,
  exponentialBackoff,
  RetryPolicy,
  Monoid,
  limitRetries
} from 'retry-ts';
import { PubSub } from '@google-cloud/pubsub';
import { pipe } from 'fp-ts/function';
import { MessageOptions } from '@google-cloud/pubsub/build/src/topic';
import Heap from 'heap';

type RetryOptions =
  | {
      delay: number;
      exponentialBackoff?: {
        capDelay?: number;
      };
      maxCumulativeDelay?: number;
      maxRetries?: number;
    }
  | false;

export type PubSubHandlerOptions = {
  projectId: string;
  topicName: string;
  includeAll?: boolean;
  attributes?: string[];
  retryOptions?: RetryOptions;
} & (
  | {
      noOrdering: true;
    }
  | {
      noOrdering?: false;
      orderingKeySuffix?: string[];
    }
);

export const pubSubHandler: (options: PubSubHandlerOptions) => XlogHandler = ({
  retryOptions = false,
  ...handlerOptions
}) => {
  const { projectId, topicName, noOrdering, includeAll } = handlerOptions;
  const pubsub = new PubSub({ projectId });

  const topic = pubsub.topic(topicName, {
    messageOrdering: !noOrdering,
    enableOpenTelemetryTracing: true
  });

  type Message = { options?: MessageOptions; walPos: bigint; priority: bigint };

  // Binary heaps for ordered messages.
  const heaps = new Map<string, Heap<Message>>();

  // Track the last WAL position so we can assign priority to control messages.
  let lastWal = 0n;

  // Track current transaction info
  let txnInfo:
    | {
        id: string;
        timeStamp: string;
      }
    | undefined;

  const getHeap = (orderingKey: string) => {
    let heap = heaps.get(orderingKey);
    if (!heap) {
      heap = new Heap<Message>((a, b) => Number(a.priority - b.priority));
      heaps.set(orderingKey, heap);
    }
    return heap;
  };

  return (xlogs) => (context) => {
    const messages: Message[] = xlogs.map(({ xlog, walPos }) => {
      const { type: eventType } = xlog;

      if (walPos > lastWal) {
        lastWal = walPos;
      }

      const priority = walPos !== 0n ? walPos : lastWal;

      let orderingKey: string | undefined;
      let attributes: Record<string, string> = { eventType };

      if (
        eventType === 'Relation' ||
        eventType === 'Insert' ||
        eventType === 'Update' ||
        eventType === 'Delete'
      ) {
        const tableName = `${xlog.namespace}.${xlog.name}`;
        const record =
          eventType === 'Insert' || eventType === 'Update'
            ? xlog.newRecord
            : eventType === 'Delete'
            ? xlog.oldRecord
            : undefined;

        attributes = txnInfo
          ? {
              ...attributes,
              tableName,
              txnId: txnInfo.id,
              txnTimeStamp: txnInfo.timeStamp
            }
          : {
              ...attributes,
              tableName
            };

        if (record && handlerOptions.attributes) {
          attributes = handlerOptions.attributes.reduce((attrs, next) => {
            const field = record[next];
            if (field) {
              return {
                ...attrs,
                [next]: stringify(field)
              };
            }
            return attrs;
          }, attributes);
        }

        if (!handlerOptions.noOrdering) {
          const { orderingKeySuffix } = handlerOptions;
          if (record && orderingKeySuffix && orderingKeySuffix.length > 0) {
            const suffix = orderingKeySuffix.reduce((suffix, next) => {
              const field = record[next];
              if (field) {
                return `${suffix}:${stringify(field)}`;
              }
              return suffix;
            }, '');

            orderingKey = `${tableName}${suffix}`;
          } else {
            orderingKey = tableName;
          }

          attributes = { ...attributes, orderingKey };
        }
      } else {
        if (eventType === 'Begin') {
          txnInfo = {
            id: xlog.tranId.toString(),
            timeStamp: new Date(
              Number(xlog.timeStamp / 1000n) + Date.UTC(2000, 0, 1)
            ).toISOString()
          };
        }

        if (eventType === 'Commit') {
          txnInfo = undefined;
        }

        if (!includeAll) {
          // No send associated with this xlog but we still need to track the WAL position.
          return { walPos, priority };
        }
      }

      const data = Buffer.from(stringify(xlog));

      const options: MessageOptions = { data, orderingKey, attributes };

      return { options, walPos, priority };
    });

    type SendXlog = TE.TaskEither<
      { error: Error; retry: SendXlog },
      { ack?: string; processed: Message }
    >;

    const makeSendXlog: (message: Message) => SendXlog = (message) => {
      const orderingKey = message.options?.orderingKey;

      const heap = orderingKey ? getHeap(orderingKey) : undefined;

      heap?.push(message);

      const sendXlog: SendXlog = () => {
        // If ordered this gets the next to be sent, else this just returns
        // the message passed in.
        const next = heap?.pop() ?? message;

        if (!next.options) {
          return Promise.resolve(E.right({ processed: next }));
        }

        return topic
          .publishMessage(next.options)
          .then((ack) => {
            return E.right({ ack, processed: next });
          })
          .catch((error: Error) => {
            heap?.push(next);
            return E.left({ error, retry: sendXlog });
          });
      };
      return sendXlog;
    };

    // Send all messages in parallel, though critically calls to publishMessage
    // will be done in messages order.
    let sendXlogs = T.sequenceArray(messages.map((msg) => makeSendXlog(msg)));

    const logger = context.logger.child({ projectId, topicName });

    // This should typically contain the WAL positions passed in, but in error/
    // retry scenarios we may end up confirming earlier or later messages that
    // were originally sent by a different invocation of this handler.
    const confirmed: bigint[] = [];

    const sendXlogsOrRetry: TE.TaskEither<Error[], bigint[]> = R.retrying(
      makeRetryPolicy(retryOptions),
      (status) => {
        const { iterNumber: retryAttempt, cumulativeDelay } = status;
        if (retryAttempt > 0) {
          logger.info(
            { retryAttempt, cumulativeDelay },
            'Retrying earlier failed send(s) of messages to topic.'
          );
        }
        return pipe(
          sendXlogs,
          T.map((results) => {
            const { left: failed, right: processed } = pipe(
              results,
              RA.partitionMap((a) => a)
            );

            const retries = failed.map(({ retry }) => retry);

            confirmed.push(...processed.map((p) => p.processed.walPos));

            const sent = processed.flatMap(({ ack, processed: { walPos } }) =>
              ack ? { ack, walPos: String(walPos) } : []
            );

            if (sent.length > 0) {
              logger.debug({ sent }, 'Sent message(s) to topic.');
            }

            if (retries.length > 0) {
              const errors = failed.map(({ error }) => error);

              logger.warn(
                { errors: errors.map((err) => err.message) },
                'Failed sending message(s) to topic.'
              );

              sendXlogs = T.sequenceArray(retries);

              return E.left(errors);
            }
            return E.right(confirmed);
          })
        );
      },
      (result) => E.isLeft(result)
    );

    return pipe(
      sendXlogsOrRetry,
      // Just return the first error.
      TE.mapLeft(([error]) => error)
    );
  };
};

const limitCumulativeDelay =
  (maxCumulativeDelay: number): RetryPolicy =>
  (status) =>
    status.cumulativeDelay > maxCumulativeDelay ? O.none : O.some(0);

const makeRetryPolicy = (retryOptions: RetryOptions) => {
  if (retryOptions) {
    let policy = retryOptions.exponentialBackoff
      ? exponentialBackoff(retryOptions.delay)
      : constantDelay(retryOptions.delay);

    if (retryOptions.exponentialBackoff?.capDelay) {
      policy = capDelay(retryOptions.exponentialBackoff.capDelay, policy);
    }

    if (retryOptions.maxCumulativeDelay) {
      policy = Monoid.concat(
        limitCumulativeDelay(retryOptions.maxCumulativeDelay),
        policy
      );
    }

    if (retryOptions.maxRetries) {
      policy = Monoid.concat(limitRetries(retryOptions.maxRetries), policy);
    }

    return policy;
  }

  return () => O.none;
};
