import { PubSub, Topic } from '@google-cloud/pubsub';
import { Logger, stringify } from '@jmorecroft67/pg-stream-core';
import { PgOutputDecoratedMessageTypes } from '@jmorecroft67/pg-stream-core';
import * as E from 'fp-ts/Either';
import * as T from 'fp-ts/Task';
import { pipe } from 'fp-ts/lib/function';
import * as RTE from 'fp-ts/ReaderTaskEither';
import { logger } from 'test-utils';
import { pubSubHandler, PubSubHandlerOptions } from './pub-sub-handler';
import * as _ from 'lodash';
import { promisify } from 'util';
jest.mock('@google-cloud/pubsub');
const PubSubMock = PubSub as jest.MockedClass<typeof PubSub>;

describe(__filename, () => {
  let topic: { publishMessage: jest.Mock };

  beforeEach(() => {
    topic = {
      publishMessage: jest.fn()
    };
    PubSubMock.prototype.topic.mockReturnValue(topic as unknown as Topic);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  const options: PubSubHandlerOptions = {
    projectId: 'test-project',
    topicName: 'test-topic',
    includeAll: false,
    noOrdering: false,
    retryOptions: {
      delay: 20,
      maxRetries: 1
    }
  };

  const makeMessage = ({ xlog }: { xlog: PgOutputDecoratedMessageTypes }) => {
    if (
      xlog.type === 'Insert' ||
      xlog.type === 'Update' ||
      xlog.type === 'Delete' ||
      xlog.type === 'Relation'
    ) {
      const orderingKey = `${xlog.namespace}.${xlog.name}`;
      const attributes = {
        orderingKey,
        tableName: orderingKey,
        eventType: xlog.type
      };

      return {
        data: Buffer.from(stringify(xlog)),
        orderingKey,
        attributes
      };
    }
  };

  const xlogs: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[] = [
    {
      xlog: {
        type: 'Insert',
        name: 'my_table',
        namespace: 'public',
        newRecord: { id: 1, word: 'x' }
      },
      walPos: 1n
    },
    {
      xlog: {
        type: 'Update',
        name: 'my_table',
        namespace: 'public',
        oldRecord: { id: 1, word: 'x' },
        newRecord: { id: 1, word: 'y' }
      },
      walPos: 2n
    },
    {
      xlog: {
        type: 'Delete',
        name: 'my_table',
        namespace: 'public',
        oldRecord: { id: 1, word: 'y' }
      },
      walPos: 3n
    }
  ];

  const xlogs2: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[] = [
    {
      xlog: {
        type: 'Insert',
        name: 'my_table',
        namespace: 'public',
        newRecord: { id: 2, word: 'x' }
      },
      walPos: 4n
    },
    {
      xlog: {
        type: 'Insert',
        name: 'my_table2',
        namespace: 'public',
        newRecord: { id: 1, word: 'x' }
      },
      walPos: 5n
    },
    {
      xlog: {
        type: 'Insert',
        name: 'my_table2',
        namespace: 'public',
        newRecord: { id: 2, word: 'x' }
      },
      walPos: 6n
    }
  ];

  const xlogs3: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[] = [
    {
      xlog: {
        type: 'Insert',
        name: 'my_table',
        namespace: 'public',
        newRecord: { id: 3, word: 'x' }
      },
      walPos: 7n
    },
    {
      xlog: {
        type: 'Insert',
        name: 'my_table2',
        namespace: 'public',
        newRecord: { id: 3, word: 'x' }
      },
      walPos: 8n
    }
  ];

  const xlogs4: { xlog: PgOutputDecoratedMessageTypes; walPos: bigint }[] = [
    {
      xlog: {
        id: 123,
        type: 'Relation',
        name: 'my_table2',
        namespace: 'public',
        replIdent: 'f',
        columns: []
      },
      walPos: 0n
    },
    {
      xlog: {
        type: 'Insert',
        name: 'my_table4',
        namespace: 'public',
        newRecord: { id: 3, word: 'x' }
      },
      walPos: 9n
    },
    {
      xlog: {
        id: 123,
        type: 'Relation',
        name: 'my_table4',
        namespace: 'public',
        replIdent: 'f',
        columns: []
      },
      walPos: 0n
    }
  ];

  it('should push messages', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockResolvedValueOnce('2');
    topic.publishMessage.mockResolvedValueOnce('3');

    const handler = pubSubHandler(options);

    const result = await handler(xlogs)({ logger })();

    expect(result).toEqual(E.right([1n, 2n, 3n]));
    expect(topic.publishMessage).toHaveBeenCalledTimes(3);
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs[2])
    );
  });

  it('should push messages, multiple calls', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockResolvedValueOnce('2');
    topic.publishMessage.mockResolvedValueOnce('3');
    topic.publishMessage.mockResolvedValueOnce('4');
    topic.publishMessage.mockResolvedValueOnce('5');
    topic.publishMessage.mockResolvedValueOnce('6');
    topic.publishMessage.mockResolvedValueOnce('7');
    topic.publishMessage.mockResolvedValueOnce('8');

    const handler = pubSubHandler(options);

    const result = await RTE.sequenceArray([
      handler(xlogs),
      handler(xlogs2),
      handler(xlogs3)
    ])({ logger })();

    expect(result).toEqual(
      E.right([
        [1n, 2n, 3n],
        [4n, 5n, 6n],
        [7n, 8n]
      ])
    );
    expect(topic.publishMessage).toHaveBeenCalledTimes(8);
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs2[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs2[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      6,
      makeMessage(xlogs2[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      7,
      makeMessage(xlogs3[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      8,
      makeMessage(xlogs3[1])
    );
  });

  it('should push control messages in correct order', async () => {
    topic.publishMessage.mockImplementation(() =>
      promisify(setTimeout)(10).then(() => '1')
    );

    const handler = pubSubHandler(options);

    const result = await RTE.sequenceArray([handler(xlogs3), handler(xlogs4)])({
      logger
    })();

    expect(result).toEqual(
      E.right([
        [7n, 8n],
        [0n, 9n, 0n]
      ])
    );

    expect(topic.publishMessage).toHaveBeenCalledTimes(5);
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs3[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs3[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs4[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs4[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs4[2])
    );
  });

  it('should handle partial failure with control messages', async () => {
    const nack = () => Promise.reject(new Error('failure'));
    const delayedAck = () => promisify(setTimeout)(10).then(() => '1');

    topic.publishMessage.mockImplementationOnce(nack);
    topic.publishMessage.mockImplementationOnce(nack);
    topic.publishMessage.mockImplementationOnce(nack);
    topic.publishMessage.mockImplementationOnce(delayedAck);
    topic.publishMessage.mockImplementationOnce(delayedAck);
    topic.publishMessage.mockImplementationOnce(delayedAck);
    topic.publishMessage.mockImplementationOnce(delayedAck);
    topic.publishMessage.mockImplementationOnce(delayedAck);

    const handler = pubSubHandler(options);

    const result = await RTE.sequenceArray([handler(xlogs3), handler(xlogs4)])({
      logger
    })();

    expect(result).toEqual(
      E.right([
        [7n, 8n],
        [9n, 0n, 0n]
      ])
    );

    expect(topic.publishMessage).toHaveBeenCalledTimes(8);
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs3[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs3[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs4[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs4[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs4[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      6,
      makeMessage(xlogs3[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      7,
      makeMessage(xlogs3[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      8,
      makeMessage(xlogs4[0])
    );
  });

  it('should handle partial failure, no retry', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockRejectedValueOnce(new Error('failed'));
    topic.publishMessage.mockResolvedValueOnce('2');

    const handler = pubSubHandler({ ...options, retryOptions: false });

    const result = await handler(xlogs)({ logger })();

    expect(result).toEqual(E.left(new Error('failed')));
    expect(topic.publishMessage).toHaveBeenCalledTimes(3);
  });

  it('should handle partial failure, succeed on retry', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockRejectedValueOnce(new Error('failed1'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed2'));
    topic.publishMessage.mockResolvedValueOnce('2');
    topic.publishMessage.mockResolvedValueOnce('3');

    const handler = pubSubHandler(options);

    const result = await handler(xlogs)({ logger })();

    expect(result).toEqual(E.right([1n, 2n, 3n]));
    expect(topic.publishMessage).toHaveBeenCalledTimes(5);

    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs[2])
    );
  });

  it('should handle partial failure, multiple calls, succeed on retry', async () => {
    topic.publishMessage.mockRejectedValueOnce(new Error('failed1'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed2'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed3'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed4'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed5'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed6'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed7'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed8'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed9'));
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockResolvedValueOnce('2');
    topic.publishMessage.mockResolvedValueOnce('3');
    topic.publishMessage.mockResolvedValueOnce('4');
    topic.publishMessage.mockResolvedValueOnce('5');
    topic.publishMessage.mockResolvedValueOnce('6');

    const handler = pubSubHandler({
      ...options,
      retryOptions: { delay: 10, maxRetries: 2 }
    });

    const result = await RTE.sequenceArray([handler(xlogs), handler(xlogs2)])({
      logger
    })();

    expect(result).toEqual(
      E.right([
        [2n, 3n, 4n],
        [1n, 5n, 6n]
      ])
    );
    expect(topic.publishMessage).toHaveBeenCalledTimes(15);

    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs2[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs2[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      6,
      makeMessage(xlogs2[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      7,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      8,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      9,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      10,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      11,
      makeMessage(xlogs2[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      12,
      makeMessage(xlogs2[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      13,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      14,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      15,
      makeMessage(xlogs2[0])
    );
  });

  it('should handle partial failure, multiple calls with delay, succeed on retry', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockRejectedValueOnce(new Error('failed1'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed2'));
    topic.publishMessage.mockResolvedValueOnce('2');
    topic.publishMessage.mockResolvedValueOnce('3');
    topic.publishMessage.mockResolvedValueOnce('4');
    topic.publishMessage.mockResolvedValueOnce('5');
    topic.publishMessage.mockResolvedValueOnce('6');
    topic.publishMessage.mockResolvedValueOnce('7');
    topic.publishMessage.mockResolvedValueOnce('8');

    const handler = pubSubHandler(options);

    const result = await RTE.sequenceArray([
      handler(xlogs),
      pipe(
        RTE.fromTask<unknown, { logger: Logger }, Error>(
          T.delay(10)(T.of(undefined))
        ),
        RTE.chain(() => RTE.sequenceArray([handler(xlogs2), handler(xlogs3)])),
        RTE.map((wals) => _.compact(wals).flat())
      )
    ])({ logger })();

    expect(result).toEqual(
      E.right([
        [1n, 4n, 7n],
        [2n, 5n, 6n, 3n, 8n]
      ])
    );
    expect(topic.publishMessage).toHaveBeenCalledTimes(10);

    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      1,
      makeMessage(xlogs[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      2,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      3,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      4,
      makeMessage(xlogs[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      5,
      makeMessage(xlogs2[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      6,
      makeMessage(xlogs2[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      7,
      makeMessage(xlogs[2])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      8,
      makeMessage(xlogs3[1])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      9,
      makeMessage(xlogs2[0])
    );
    expect(topic.publishMessage).toHaveBeenNthCalledWith(
      10,
      makeMessage(xlogs3[0])
    );
  });

  it('should handle partial failure, fail on retry', async () => {
    topic.publishMessage.mockResolvedValueOnce('1');
    topic.publishMessage.mockRejectedValueOnce(new Error('failed1'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed2'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed3'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed4'));

    const handler = pubSubHandler(options);

    const result = await handler(xlogs)({ logger })();

    expect(result).toEqual(E.left(new Error('failed3')));
    expect(topic.publishMessage).toHaveBeenCalledTimes(5);
  });

  it('should handle immediate failure, fail on retry', async () => {
    topic.publishMessage.mockRejectedValueOnce(new Error('failed1'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed2'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed3'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed4'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed5'));
    topic.publishMessage.mockRejectedValueOnce(new Error('failed6'));

    const handler = pubSubHandler(options);

    const result = await handler(xlogs)({ logger })();

    expect(result).toEqual(E.left(new Error('failed4')));
    expect(topic.publishMessage).toHaveBeenCalledTimes(6);
  });
});
