import * as pg from '@jmorecroft67/pg-stream-core';
import * as gcp from '../../src';
import { pipe } from 'fp-ts/function';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as E from 'fp-ts/Either';
import * as T from 'fp-ts/Task';
import * as TE from 'fp-ts/TaskEither';
import { Message, PubSub, Subscription, Topic } from '@google-cloud/pubsub';
import { clearDb, getOptionsFromEnv } from 'test-utils';
import { createLogger } from 'bunyan';
import { env } from 'process';

describe(__filename, () => {
  let pubsub: PubSub;
  let topic: Topic;
  let sub: Subscription;
  let options: pg.RecvlogicalOptions;

  // Set this low if we're running in a CI context.
  const MESSAGE_COUNT = env.CI ? 1000 : 10000;

  beforeAll(() => {
    const projectId = process.env['PUBSUB_PROJECT_ID'];
    if (!projectId) {
      fail();
    }
    pubsub = new PubSub({ projectId });
  });

  beforeEach(async () => {
    [topic] = await pubsub.createTopic({ name: 'test-gcp-perf-topic' });
    [sub] = await topic.createSubscription('test-gcp-perf-desc', {
      enableMessageOrdering: true
    });
  });
  beforeEach(() => {
    const env = getOptionsFromEnv();

    if (E.isLeft(env)) {
      fail(env.left);
    }

    options = {
      ...env.right,
      slotName: 'test_gcp_perf_slot',
      publicationName: 'test_gcp_perf_pub',
      createPublicationIfNone: true,
      createTempSlotIfNone: true,
      xlogs: {
        handler: gcp.pubSubHandler({
          projectId: pubsub.projectId,
          topicName: topic.name
        }),
        concurrency: 5
      },
      endAfter: {
        duration: 15000
      }
    };
  });

  afterEach(clearDb);
  afterEach(async () => {
    await sub?.delete();
    await topic?.delete();
  });

  it('should push updates to pubsub', async () => {
    // Don't use the usual logger for this test since it's debug level and will produce too much
    // output.
    const logger = createLogger({
      level: 'info',
      name: 'test_gcp_perf'
    });

    const doStuff = TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, true),
            RTE.chain(() =>
              RTE.sequenceArray([
                pg.query(
                  'CREATE TABLE gcp_perf1_test (id INT PRIMARY KEY, word VARCHAR)'
                ),
                pg.query(
                  'CREATE TABLE gcp_perf2_test (id INT PRIMARY KEY, word VARCHAR)'
                ),
                pg.query(
                  `INSERT INTO gcp_perf1_test SELECT g.*, 'word' FROM generate_series(1, ${MESSAGE_COUNT}, 1) AS g(series)`
                ),
                pg.query(
                  `INSERT INTO gcp_perf2_test SELECT g.*, 'word' FROM generate_series(1, ${MESSAGE_COUNT}, 1) AS g(series)`
                )
              ])
            )
          )
        ),
      (conn) => pg.close({ conn, logger })
    );

    const recvlogical = TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, true),
            RTE.chain(() => pg.recvlogical(options))
          )
        ),
      (conn) => pg.close({ conn, logger })
    );

    const counts: Record<string, number> = {
      gcp_perf1_test: 0,
      gcp_perf2_test: 0
    };

    sub.on('message', (message: Message) => {
      const data = JSON.parse(message.data.toString());
      if (data.type === 'Insert') {
        const table: string = data.name;
        expect(data.newRecord.id).toEqual(counts[table] + 1);
        expect(data.newRecord.word).toEqual('word');
        counts[table] += 1;
      }
      message.ack();
    });

    await TE.sequenceArray([
      T.delay(1000)(
        pipe(
          doStuff,
          TE.map((): void => undefined)
        )
      ),
      recvlogical
    ])();

    expect(counts['gcp_perf1_test']).toEqual(MESSAGE_COUNT);
    expect(counts['gcp_perf2_test']).toEqual(MESSAGE_COUNT);
  }, 20000);
});
