import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as T from 'fp-ts/Task';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as pg from '@jmorecroft67/pg-stream-core';
import { logger, clearDb, getOptionsFromEnv } from 'test-utils';
import { fail } from 'assert';
import { pubSubHandler } from '../../src';
import { Message, PubSub, Subscription, Topic } from '@google-cloud/pubsub';

describe(__filename, () => {
  let pubsub: PubSub;
  let topic: Topic;
  let sub: Subscription;
  let options: pg.RecvlogicalOptions;

  beforeAll(() => {
    const projectId = process.env['PUBSUB_PROJECT_ID'];
    if (!projectId) {
      fail();
    }
    pubsub = new PubSub({ projectId });
  });

  beforeEach(async () => {
    [topic] = await pubsub.createTopic({ name: 'test-gcp-topic' });
    [sub] = await topic.createSubscription('test-gcp-desc', {
      enableMessageOrdering: true
    });
  });
  beforeEach(() => {
    const env = getOptionsFromEnv();

    if (E.isLeft(env)) {
      fail(env.left);
    }

    options = {
      ...env.right,
      slotName: 'test_gcp_slot',
      publicationName: 'test_gcp_pub',
      includeTables: ['test_gcp'],
      createPublicationIfNone: true,
      createTempSlotIfNone: true,
      xlogs: {
        handler: pubSubHandler({
          projectId: pubsub.projectId,
          topicName: topic.name
        }),
        concurrency: 1
      },
      endAfter: {
        duration: 3000
      }
    };
  });

  afterEach(clearDb);
  afterEach(async () => {
    await sub?.delete();
    await topic?.delete();
  });

  it('should sync table insert', (done) => {
    const createTable = pg.query(
      `CREATE TABLE test_gcp (id INTEGER PRIMARY KEY, word VARCHAR NOT NULL, flag BOOLEAN, matrix INT[][], jason JSON)`
    );

    const populateTable = TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, true),
            RTE.chain(() =>
              pg.query('ALTER TABLE test_gcp REPLICA IDENTITY FULL')
            ),
            RTE.chain(() =>
              pg.query(
                `INSERT INTO test_gcp VALUES 
                  (1, 'hello', null, null, '{"meaning":[42]}'), 
                  (2, 'gday', true, array[array[1,2,3], array[4,5,6]], null)`
              )
            ),
            RTE.chain(() =>
              pg.query(`UPDATE test_gcp SET word = 'hiya' WHERE id = 1`)
            ),
            RTE.chain(() => pg.query(`DELETE FROM test_gcp WHERE id = 1`))
          )
        ),
      (conn) => pg.close({ conn, logger })
    );

    const data: object[] = [];
    const attributes: Record<string, string>[] = [];
    sub.on('message', (message: Message) => {
      data.push(JSON.parse(message.data.toString()));
      attributes.push(message.attributes);
      message.ack();
    });

    TE.bracket(
      pg.open({ options, logger }),
      (conn) =>
        pipe(
          { conn, logger },
          pipe(
            pg.start(options, true),
            RTE.chain(() => createTable),
            RTE.chain(() =>
              RTE.sequenceArray([
                pg.recvlogical(options),
                RTE.fromTaskEither(
                  pipe(
                    T.delay(1000)(T.of(E.of(undefined))),
                    TE.chain(() => populateTable),
                    TE.map(() => undefined)
                  )
                )
              ])
            )
          )
        ),
      (conn) => pg.close({ conn, logger })
    )().then((result) => {
      pipe(
        result,
        E.fold(
          (error) => {
            done(error);
          },
          () => {
            expect(attributes).toEqual([
              {
                eventType: 'Relation',
                orderingKey: 'public.test_gcp',
                tableName: 'public.test_gcp',
                txnId: expect.any(String),
                txnTimeStamp: expect.any(String)
              },
              {
                eventType: 'Insert',
                orderingKey: 'public.test_gcp',
                tableName: 'public.test_gcp',
                txnId: expect.any(String),
                txnTimeStamp: expect.any(String)
              },
              {
                eventType: 'Insert',
                orderingKey: 'public.test_gcp',
                tableName: 'public.test_gcp',
                txnId: expect.any(String),
                txnTimeStamp: expect.any(String)
              },
              {
                eventType: 'Update',
                orderingKey: 'public.test_gcp',
                tableName: 'public.test_gcp',
                txnId: expect.any(String),
                txnTimeStamp: expect.any(String)
              },
              {
                eventType: 'Delete',
                orderingKey: 'public.test_gcp',
                tableName: 'public.test_gcp',
                txnId: expect.any(String),
                txnTimeStamp: expect.any(String)
              }
            ]);

            expect(data).toEqual([
              expect.objectContaining({
                type: 'Relation',
                name: 'test_gcp',
                namespace: 'public',
                columns: [
                  expect.objectContaining({ name: 'id', dataTypeName: 'int4' }),
                  expect.objectContaining({
                    name: 'word',
                    dataTypeName: 'varchar'
                  }),
                  expect.objectContaining({
                    name: 'flag',
                    dataTypeName: 'bool'
                  }),
                  expect.objectContaining({
                    name: 'matrix',
                    dataTypeName: 'int4[]'
                  }),
                  expect.objectContaining({
                    name: 'jason',
                    dataTypeName: 'json'
                  })
                ]
              }),
              expect.objectContaining({
                type: 'Insert',
                namespace: 'public',
                name: 'test_gcp',
                newRecord: {
                  id: 1,
                  word: 'hello',
                  flag: null,
                  matrix: null,
                  jason: {
                    meaning: [42]
                  }
                }
              }),
              expect.objectContaining({
                type: 'Insert',
                namespace: 'public',
                name: 'test_gcp',
                newRecord: {
                  id: 2,
                  word: 'gday',
                  flag: true,
                  matrix: [
                    [1, 2, 3],
                    [4, 5, 6]
                  ],
                  jason: null
                }
              }),
              expect.objectContaining({
                type: 'Update',
                namespace: 'public',
                name: 'test_gcp',
                newRecord: {
                  id: 1,
                  word: 'hiya',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                },
                oldRecord: {
                  id: 1,
                  word: 'hello',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                }
              }),
              expect.objectContaining({
                namespace: 'public',
                name: 'test_gcp',
                type: 'Delete',
                oldRecord: {
                  id: 1,
                  word: 'hiya',
                  flag: null,
                  matrix: null,
                  jason: { meaning: [42] }
                }
              })
            ]);

            done();
          }
        )
      );
    });
  }, 10000);
});
