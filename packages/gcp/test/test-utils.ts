import { createLogger } from 'bunyan';
import { pipe } from 'fp-ts/function';
import * as E from 'fp-ts/Either';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import * as pg from '@jmorecroft67/pg-stream-core';
import { BooleanFromString, IntFromString } from 'io-ts-types';
import { env } from 'process';
import { failure } from 'io-ts/PathReporter';

export const logger = createLogger({
  name: 'test',
  level: 'debug'
});

export const getOptionsFromEnv: (
  defaults?: true
) => E.Either<Error, pg.BaseOptions> = (defaults) => {
  const codec = t.type({
    host: t.string,
    port: IntFromString,
    username: t.string,
    password: t.string,
    database: t.string,
    useSSL: t.union([BooleanFromString, t.undefined])
  });

  return pipe(
    codec.decode({
      host: env['PGHOST'] ?? (defaults && 'localhost'),
      port: env['PGPORT'] ?? (defaults && '5432'),
      username: env['PGUSER'] ?? (defaults && env['LOGNAME']),
      password: env['PGPASSWORD'],
      database: env['PGDATABASE'] ?? (defaults && 'postgres'),
      useSSL: env['PG_STREAM_USE_SSL']
    }),
    E.mapLeft((e) => new Error(failure(e).join('\n')))
  );
};

export const clearDb: (done: jest.DoneCallback) => void = (done) => {
  pipe(
    TE.fromEither(getOptionsFromEnv()),
    TE.chain((options) =>
      TE.bracket(
        pg.open({ options, logger }),
        (conn) =>
          pipe(
            { conn, logger },
            pipe(
              pg.start(options, false),
              RTE.chain(() =>
                pipe(
                  pg.queryAndDecode(
                    'SELECT pubname FROM pg_publication',
                    t.array(t.type({ pubname: t.string }))
                  ),
                  RTE.map((pubs) => pubs.map(({ pubname }) => pubname))
                )
              ),
              RTE.bindTo('pubs'),
              RTE.bind('tables', () =>
                pipe(
                  pg.queryAndDecode(
                    `SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE'`,
                    t.array(t.type({ table_name: t.string }))
                  ),
                  RTE.map((tables) =>
                    tables.map(({ table_name }) => table_name)
                  )
                )
              ),
              RTE.chain(({ pubs, tables }) =>
                pipe(
                  pg.query(
                    'SELECT pg_drop_replication_slot(slot_name) FROM pg_replication_slots'
                  ),
                  RTE.chain(() =>
                    RTE.sequenceArray(
                      pubs.map((pub) =>
                        pg.query(`DROP PUBLICATION IF EXISTS ${pub}`)
                      )
                    )
                  ),
                  RTE.chain(() =>
                    RTE.sequenceArray(
                      tables.map((table) =>
                        pg.query(`DROP TABLE IF EXISTS ${table} CASCADE`)
                      )
                    )
                  )
                )
              )
            )
          ),
        (conn) => pg.close({ conn, logger })
      )
    )
  )().then((result) =>
    pipe(
      result,
      E.fold(done, () => done())
    )
  );
};
