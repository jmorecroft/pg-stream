# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.20.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.19.0...v0.20.0) (2022-07-12)


### Features

* Add basic support for OpenTelemetry tracing with GCP PubSub. ([f46d5f3](https://gitlab.com/jmorecroft/pg-stream/commit/f46d5f3ce42ca98da975ee3a295a2965e7394f12))





# [0.19.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.3...v0.19.0) (2022-06-26)


### Features

* **gcp:** Added txn info (id, timestamp) to message attributes. ([bd0c048](https://gitlab.com/jmorecroft/pg-stream/commit/bd0c048b1bd6a5c150e7f48715dbd8653c15e946))





## [0.18.3](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.2...v0.18.3) (2022-06-01)


### Bug Fixes

* Fix test utils. ([55f7eb9](https://gitlab.com/jmorecroft/pg-stream/commit/55f7eb9a6f6d00bb8f6ab49452389583536510ed))
* Further fix to handling of control messages in core and gcp libs. ([09aafe8](https://gitlab.com/jmorecroft/pg-stream/commit/09aafe80750722886004906c62add2a2f864c038))





## [0.18.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.1...v0.18.2) (2022-06-01)

**Note:** Version bump only for package @jmorecroft67/pg-stream-gcp





## [0.18.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.18.0...v0.18.1) (2022-06-01)

**Note:** Version bump only for package @jmorecroft67/pg-stream-gcp





# [0.18.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.17.1...v0.18.0) (2022-05-24)


### Features

* **gcp:** Added orderingKey to message attributes. ([11c3a0f](https://gitlab.com/jmorecroft/pg-stream/commit/11c3a0f1aea13169447a65519da8bd224297ecc0))





## [0.17.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.17.0...v0.17.1) (2022-05-23)

**Note:** Version bump only for package @jmorecroft67/pg-stream-gcp





# [0.17.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.16.1...v0.17.0) (2022-05-23)


### Features

* Added options to GCP handler (and corresponding CLI) to set custom attributes and ordering key suffix fields. ([58bab4e](https://gitlab.com/jmorecroft/pg-stream/commit/58bab4e603df197380c6805043c437eb022dc76b))





# [0.16.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.15.0...v0.16.0) (2022-05-21)


### Features

* **cli:** Added optional retry to recvlogical and recvlogical-gcp commands. ([69a8218](https://gitlab.com/jmorecroft/pg-stream/commit/69a821892649587fc5d7e6eec48eafe2e842d528))





# [0.15.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.14.2...v0.15.0) (2022-05-20)


* feat(core)!: Simplified query function by removing decode, added new convenience function queryAndDecode for doing query and additional decode step. ([2e89eb2](https://gitlab.com/jmorecroft/pg-stream/commit/2e89eb24942e81ae34306f188880950103297185))


### BREAKING CHANGES

* query function no longer takes decoder input, must use queryAndDecode instead.





## [0.14.2](https://gitlab.com/jmorecroft/pg-stream/compare/v0.14.1...v0.14.2) (2022-05-19)

**Note:** Version bump only for package @jmorecroft67/pg-stream-gcp





## [0.14.1](https://gitlab.com/jmorecroft/pg-stream/compare/v0.14.0...v0.14.1) (2022-05-19)

**Note:** Version bump only for package @jmorecroft67/pg-stream-gcp





# [0.14.0](https://gitlab.com/jmorecroft/pg-stream/compare/v0.13.3...v0.14.0) (2022-05-19)


* feat!: Added new package gcp containing a handler for pushing to GCP PubSub. ([29555b4](https://gitlab.com/jmorecroft/pg-stream/commit/29555b434bcd4c21b8e9a57d28b4f1834bb23bf9))


### BREAKING CHANGES

* Changed handler interface to allow passing in context (currently just contains a logger).
